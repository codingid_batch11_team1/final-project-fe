import { BrowserRouter, Route, Routes, useLocation } from "react-router-dom";
import RegisterPage from "./page/RegisterPage";
import RealHeader from "./component/RealHeader";
import EmailConfirmSuccessPage from "./page/EmailConfirmSuccessPage";
import LoginPage from "./page/LoginPage";
import ResetPassPage from "./page/ResetPassPage";
import NewPassPage from "./page/NewPassPage";
import LandingPage from "./page/LandingPage";
import LandingListMenuClassPage from "./page/LandingListMenuClassPage";
import InvoicePage from "./page/InvoicePage";
import InvoiceDetailPage from "./page/InvoiceDetailPage";
import ClassDetailPage from "./page/ClassDetailPage";
import CheckoutPage from "./page/CheckoutPage";
import { ThemeProvider } from "@mui/material";
import Theme from "./component/Theme";
import "./index.css";
import DashboardAdmin from "./page/Admin/DashboardAdmin";
import MyClassPage from "./page/MyClassPage";
import PurchaseSuccessPage from "./page/PurchaseSuccessPage"
import { NonAdminWrapper, GuestWrapper, UserWrapper, AdminWrapper } from "./RouteWrapper"
import InvoiceDetailContent from "./page/Admin/InvoiceDetailContent";

function AppContent() {
  const location = useLocation();
  const shouldShowNavbar =
    location.pathname !== "/dashboardadmin" &&
    !location.pathname.startsWith("/dashboardadmin/invoicedetail/");

  return (

    <>
      {shouldShowNavbar && <RealHeader />}
      <Routes>
        {/* These routes can be accessed by non-users and users but not by admins */}
        <Route path="/" element={<NonAdminWrapper><LandingPage /></NonAdminWrapper>} />
        <Route path="/landing_list_menu_class_page/:idCategory" element={<NonAdminWrapper><LandingListMenuClassPage /></NonAdminWrapper>} />
        <Route path="/class_detail_buy/:idCategory/:idCourse" element={<NonAdminWrapper><ClassDetailPage /></NonAdminWrapper>} />

        {/* These routes can only be accessed by guests (non-users) */}
        <Route path="/login" element={<GuestWrapper><LoginPage /></GuestWrapper>} />
        <Route path="/register" element={<GuestWrapper><RegisterPage /></GuestWrapper>} />
        <Route path="/emailConfirmSuccess" element={<GuestWrapper><EmailConfirmSuccessPage /></GuestWrapper>} />
        <Route path="/resetPassword" element={<GuestWrapper><ResetPassPage /></GuestWrapper>} />
        <Route path="/newPass" element={<GuestWrapper><NewPassPage /></GuestWrapper>} />

        {/* These routes can only be accessed by authenticated users (non-admins) */}
        <Route path="/checkout" element={<UserWrapper><CheckoutPage /></UserWrapper>} />
        <Route path="/purchase-success" element={<UserWrapper><PurchaseSuccessPage /></UserWrapper>} />
        <Route path="/invoice" element={<UserWrapper><InvoicePage /></UserWrapper>} />
        <Route path="/invoice_detail/:idInvoice" element={<UserWrapper><InvoiceDetailPage /></UserWrapper>} />
        <Route path="/myclass" element={<UserWrapper><MyClassPage /></UserWrapper>} />

        {/* This route can only be accessed by admin users */}
        <Route path="/dashboardadmin" element={<AdminWrapper><DashboardAdmin /></AdminWrapper>} />
        <Route path="/dashboardadmin/invoicedetail/:idInvoice" element={<AdminWrapper><InvoiceDetailContent /></AdminWrapper>} />
      </Routes>
    </>

  );
}

function App() {
  return (
    <BrowserRouter>
      <ThemeProvider theme={Theme}>
        <AppContent />
      </ThemeProvider>
    </BrowserRouter>
  );
}


export default App;

