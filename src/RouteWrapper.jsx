import { useNavigate } from "react-router-dom";
import { useContext, useEffect } from "react";
import { AuthContext } from "./contexts/AuthContext";

export const NonAdminWrapper = ({ children }) => {
    const { role } = useContext(AuthContext);
    const navigate = useNavigate();

    useEffect(() => {
        if (role === 'ADMIN') {
            navigate("/dashboardadmin");
        }
    }, [role, navigate]);

    return children;
};

export const GuestWrapper = ({ children }) => {
    const { isAuth } = useContext(AuthContext);
    const navigate = useNavigate();

    useEffect(() => {
        if (isAuth) {
            navigate("/");
        }
    }, [isAuth, navigate]);

    return children;
};

export const UserWrapper = ({ children }) => {
    const { isAuth, role } = useContext(AuthContext);
    const navigate = useNavigate();

    useEffect(() => {
        if (!isAuth) {
            navigate("/login");
        } else if (role === 'ADMIN') {
            navigate("/dashboardadmin");
        }
    }, [isAuth, role, navigate]);

    return children;
};

export const AdminWrapper = ({ children }) => {
    const { role } = useContext(AuthContext);
    const navigate = useNavigate();

    useEffect(() => {
        if (role !== 'ADMIN') {
            navigate("/");
        }
    }, [role, navigate]);

    return children;
};
