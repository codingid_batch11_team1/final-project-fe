import { useContext, useState } from "react";
import { styled } from "@mui/material/styles";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell, { tableCellClasses } from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import { Button } from "@mui/material";
import axios from "axios";
import Radio from "@mui/material/Radio";
import RadioGroup from "@mui/material/RadioGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  TextField,
  Box,
  Typography,
} from "@mui/material";
import useGetCategoryByAdmin from "../Hooks/useGetCategoryByAdmin";
import { AuthContext } from "../../contexts/AuthContext";

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: "#5B4947",
    color: theme.palette.common.white,
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));

const StyledTableRow = styled(TableRow)(() => ({
  "&:nth-of-type(even)": {
    backgroundColor: "rgba(91, 73, 71, 0.2)",
  },
  // hide last border
  "&:last-child td, &:last-child th": {
    border: 0,
  },
}));

export default function PaymentTableAdmin() {
  //   const [paymentsData, setPaymentsData] = useState([]);
  const [selectedCategory, setSelectedCategory] = useState(null);
  const [isDialogOpen, setIsDialogOpen] = useState(false);
  const [loading, setLoading] = useState(true);

  const { data } = useGetCategoryByAdmin();

  const { token } = useContext(AuthContext);

  const handleInputChange = (e) => {
    const { name, type } = e.target;
    let value;

    if (type === "checkbox" || type === "boolean") {
      value = e.target.value;
    } else if (type === "radio") {
      value = e.target.value === "true";
    } else {
      value = e.target.value;
    }

    const updatedCategory = { ...selectedCategory, [name]: value };
    setSelectedCategory(updatedCategory);
  };

  const handleSave = async () => {
    setLoading(true);

    try {
      const editUrl = `${import.meta.env.VITE_API_URL}/api/CourseCategory?id=${
        selectedCategory.idCategory
      }`;
      const formData = new FormData();
      formData.append("categoryName", selectedCategory.categoryName);
      formData.append("categoryDetail", selectedCategory.categoryDetail);
      formData.append("categoryImage", selectedCategory.categoryImage);
      formData.append("categoryBanner", selectedCategory.categoryBanner);
      formData.append("isActive", selectedCategory.categoryStatus);

      const response = await axios.patch(editUrl, formData, {
        headers: {
          "Content-Type": "multipart/form-data",
          Authorization: `Bearer ${token}`,
        },
      });

      // console.log("Category edited successfully:", response.data);
      window.alert("Category has been edit !!");
      window.location.reload();
      setIsDialogOpen(false); // Close the dialog after saving
      setLoading(false);
    } catch (error) {
      console.error("Error editing Category:", error);
    }
  };

  return (
    <TableContainer component={Paper}>
      <Table sx={{ minWidth: 700 }} aria-label="customized table">
        <TableHead
          sx={{
            width: "1140px",
            height: "60px",
            padding: "20px, 20px, 20px, 0px",
            gap: "24px",
          }}
        >
          <TableRow>
            <StyledTableCell
              sx={{
                fontWeight: 600,
                fontFamily: "Montserrat",
                fontSize: "16px",
                lineHeight: "19.5px",
                color: "#FFFFFF",
              }}
            >
              No
            </StyledTableCell>
            <StyledTableCell
              align="center"
              sx={{
                fontWeight: 600,
                fontFamily: "Montserrat",
                fontSize: "16px",
                lineHeight: "19.5px",
                color: "#FFFFFF",
              }}
            >
              Category Name
            </StyledTableCell>
            <StyledTableCell
              align="center"
              sx={{
                fontWeight: 600,
                fontFamily: "Montserrat",
                fontSize: "16px",
                lineHeight: "19.5px",
                color: "#FFFFFF",
              }}
            >
              Category Detail
            </StyledTableCell>
            <StyledTableCell
              align="center"
              sx={{
                fontWeight: 600,
                fontFamily: "Montserrat",
                fontSize: "16px",
                lineHeight: "19.5px",
                color: "#FFFFFF",
              }}
            >
              Category Image
            </StyledTableCell>
            <StyledTableCell
              align="center"
              sx={{
                fontWeight: 600,
                fontFamily: "Montserrat",
                fontSize: "16px",
                lineHeight: "19.5px",
                color: "#FFFFFF",
              }}
            >
              Category Banner
            </StyledTableCell>
            <StyledTableCell
              align="center"
              sx={{
                fontWeight: 600,
                fontFamily: "Montserrat",
                fontSize: "16px",
                lineHeight: "19.5px",
                color: "#FFFFFF",
              }}
            >
              Category Status
            </StyledTableCell>
            <StyledTableCell
              align="center"
              sx={{
                fontWeight: 600,
                fontFamily: "Montserrat",
                fontSize: "16px",
                lineHeight: "19.5px",
                color: "#FFFFFF",
              }}
            >
              Action
            </StyledTableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {data.map((row, index) => {
            let status = "";
            if (row.isActive) {
              status = "Active";
            } else {
              status = "Inactive";
            }
            return (
              <StyledTableRow key={row.idCategory}>
                <StyledTableCell
                  component="th"
                  scope="row"
                  sx={{
                    fontWeight: 500,
                    fontFamily: "Montserrat",
                    fontSize: "16px",
                    lineHeight: "19.5px",
                    color: "#4F4F4F",
                  }}
                >
                  {index + 1}
                </StyledTableCell>
                <StyledTableCell
                  align="center"
                  sx={{
                    fontWeight: 500,
                    fontFamily: "Montserrat",
                    fontSize: "16px",
                    lineHeight: "19.5px",
                    color: "#4F4F4F",
                  }}
                >
                  {row.categoryName}
                </StyledTableCell>
                <StyledTableCell
                  align="center"
                  sx={{
                    fontWeight: 500,
                    fontFamily: "Montserrat",
                    fontSize: "16px",
                    lineHeight: "19.5px",
                    color: "#4F4F4F",
                  }}
                >
                  {row.categoryDetail}
                </StyledTableCell>
                <StyledTableCell align="center">
                  <img
                    src={`${import.meta.env.VITE_API_URL}/${row.categoryImage}`}
                    alt=""
                    style={{ width: "40px", height: "40px" }}
                  />
                </StyledTableCell>
                <StyledTableCell align="center">
                  <img
                    src={`${import.meta.env.VITE_API_URL}/${
                      row.categoryBanner
                    }`}
                    alt=""
                    style={{ width: "40px", height: "40px" }}
                  />
                </StyledTableCell>
                <StyledTableCell
                  align="center"
                  sx={{
                    fontWeight: 500,
                    fontFamily: "Montserrat",
                    fontSize: "16px",
                    lineHeight: "19.5px",
                    color: "#4F4F4F",
                  }}
                >
                  {status}
                </StyledTableCell>
                <StyledTableCell align="center">
                  <Button
                    sx={{
                      padding: "10px",
                      background: "#FABC1D",
                      color: "#5B4947",
                      borderRadius: "8px",
                      width: "180px",
                      height: "37px",
                      fontWeight: 700,
                      fontFamily: "Montserrat",
                      fontSize: "14px",
                      lineHeight: "17.07px",
                      textTransform: "capitalize",
                    }}
                    onClick={() => {
                      setSelectedCategory(row);
                      setIsDialogOpen(true);
                    }}
                  >
                    Edit
                  </Button>
                </StyledTableCell>
              </StyledTableRow>
            );
          })}
        </TableBody>
      </Table>
      {selectedCategory && (
        <Dialog open={isDialogOpen} onClose={() => setIsDialogOpen(false)}>
          <DialogTitle>Edit Course</DialogTitle>
          <DialogContent>
            <Box display="flex" flexDirection="column">
              <TextField
                label="Category Name"
                value={selectedCategory.categoryName}
                onChange={handleInputChange}
                name="categoryName"
                sx={{ marginTop: "10px" }}
              />
              <TextField
                label="Category Detail"
                value={selectedCategory.categoryDetail}
                onChange={handleInputChange}
                name="categoryDetail"
                sx={{ marginTop: "10px" }}
              />
              <div>
                <Typography id="modal-modal-description" sx={{ mt: 1 }}>
                  Image
                </Typography>
                <div style={{ marginTop: "5px" }}>
                  <TextField
                    type="file"
                    size="small"
                    variant="outlined"
                    onChange={(e) =>
                      setSelectedCategory({
                        ...selectedCategory,
                        categoryImage: e.target.files[0],
                      })
                    }
                  />
                  <Typography id="modal-modal-description" sx={{ mt: 1 }}>
                    Banner
                  </Typography>
                  <TextField
                    type="file"
                    size="small"
                    variant="outlined"
                    onChange={(e) =>
                      setSelectedCategory({
                        ...selectedCategory,
                        categoryBanner: e.target.files[0],
                      })
                    }
                  />
                </div>
              </div>
              <div>
                <Typography id="modal-modal-description" sx={{ mt: 1 }}>
                  Status
                </Typography>
                <div style={{ marginTop: "5px" }}>
                  <RadioGroup
                    row
                    aria-labelledby="demo-row-radio-buttons-group-label"
                    name="categoryStatus" // Field name in selectedUser object
                    value={selectedCategory.categoryStatus} // Convert to string for RadioGroup
                    onChange={handleInputChange}
                  >
                    <FormControlLabel
                      value={true}
                      control={<Radio />}
                      label="Active"
                    />
                    <FormControlLabel
                      value={false}
                      control={<Radio />}
                      label="Inactive"
                    />
                  </RadioGroup>
                </div>
              </div>
            </Box>
          </DialogContent>
          <DialogActions>
            <Button onClick={() => setIsDialogOpen(false)}>Cancel</Button>
            <Button onClick={handleSave}>Save</Button>
          </DialogActions>
        </Dialog>
      )}
    </TableContainer>
  );
}
