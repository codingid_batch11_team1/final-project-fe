import { useContext, useState } from "react";
import { styled } from "@mui/material/styles";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell, { tableCellClasses } from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import { Button } from "@mui/material";
import axios from "axios";
import Radio from "@mui/material/Radio";
import RadioGroup from "@mui/material/RadioGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";

import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  TextField,
  Box,
  Typography,
} from "@mui/material";
import useGetCourseByAdmin from "../Hooks/useGetCourseByAdmin";
import useGetCategory from "../Hooks/useGetCategory";
import { AuthContext } from "../../contexts/AuthContext";

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: "#5B4947",
    color: theme.palette.common.white,
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));

const StyledTableRow = styled(TableRow)(() => ({
  "&:nth-of-type(even)": {
    backgroundColor: "rgba(91, 73, 71, 0.2)",
  },
  // hide last border
  "&:last-child td, &:last-child th": {
    border: 0,
  },
}));

export default function PaymentTableAdmin() {
  //   const [paymentsData, setPaymentsData] = useState([]);
  const [selectedCourse, setSelectedCourse] = useState(null);
  const [isDialogOpen, setIsDialogOpen] = useState(false);

  const { courses } = useGetCourseByAdmin();

  const { data } = useGetCategory();

  const { token } = useContext(AuthContext);

  const handleInputChange = (e) => {
    const { name, type } = e.target;
    let value;

    if (type === "checkbox" || type === "boolean") {
      value = e.target.value;
    } else if (type === "radio") {
      value = e.target.value === "true";
    } else {
      value = e.target.value;
    }

    const updatedCourse = { ...selectedCourse, [name]: value };
    setSelectedCourse(updatedCourse);
  };

  const handleSave = async () => {
    try {
      const editUrl = `${import.meta.env.VITE_API_URL}/api/Course?id=${
        selectedCourse.courseId
      }`;
      const formData = new FormData();
      formData.append("courseName", selectedCourse.courseName);
      formData.append("courseDetail", selectedCourse.courseDetail);
      formData.append("coursePrice", selectedCourse.coursePrice);
      formData.append("courseImage", selectedCourse.courseImage);
      formData.append("fk_Id_Category", selectedCourse.courseCategory);
      formData.append("isActive", selectedCourse.courseStatus);

      const response = await axios.patch(editUrl, formData, {
        headers: {
          "Content-Type": "multipart/form-data",
          Authorization: `Bearer ${token}`,
        },
      });

      // console.log("Course edited successfully:", response.data);
      window.alert("Course has been edit !!");
      window.location.reload();
      setIsDialogOpen(false); // Close the dialog after saving
    } catch (error) {
      console.error("Error editing Course:", error);
    }
  };

  // console.log("ini course name: ", selectedCourse.courseName)
  // console.log("ini course detail: ", selectedCourse.courseDetail)
  // console.log("ini course harga: ", selectedCourse.coursePrice)
  // console.log("ini course gambar: ", selectedCourse.courseImage)
  // console.log("ini course kategori: ", selectedCourse.courseCategory)
  // console.log("ini course status: ", selectedCourse.courseStatus)

  return (
    <TableContainer component={Paper}>
      <Table sx={{ minWidth: 700 }} aria-label="customized table">
        <TableHead
          sx={{
            width: "1140px",
            height: "60px",
            padding: "20px, 20px, 20px, 0px",
            gap: "24px",
          }}
        >
          <TableRow>
            <StyledTableCell
              sx={{
                fontWeight: 600,
                fontFamily: "Montserrat",
                fontSize: "16px",
                lineHeight: "19.5px",
                color: "#FFFFFF",
              }}
            >
              No
            </StyledTableCell>
            <StyledTableCell
              align="center"
              sx={{
                fontWeight: 600,
                fontFamily: "Montserrat",
                fontSize: "16px",
                lineHeight: "19.5px",
                color: "#FFFFFF",
              }}
            >
              Course Name
            </StyledTableCell>
            <StyledTableCell
              align="center"
              sx={{
                fontWeight: 600,
                fontFamily: "Montserrat",
                fontSize: "16px",
                lineHeight: "19.5px",
                color: "#FFFFFF",
              }}
            >
              Course Detail
            </StyledTableCell>
            <StyledTableCell
              align="center"
              sx={{
                fontWeight: 600,
                fontFamily: "Montserrat",
                fontSize: "16px",
                lineHeight: "19.5px",
                color: "#FFFFFF",
              }}
            >
              Course Category
            </StyledTableCell>
            <StyledTableCell
              align="center"
              sx={{
                fontWeight: 600,
                fontFamily: "Montserrat",
                fontSize: "16px",
                lineHeight: "19.5px",
                color: "#FFFFFF",
              }}
            >
              Course Price
            </StyledTableCell>
            <StyledTableCell
              align="center"
              sx={{
                fontWeight: 600,
                fontFamily: "Montserrat",
                fontSize: "16px",
                lineHeight: "19.5px",
                color: "#FFFFFF",
              }}
            >
              Course Image
            </StyledTableCell>
            <StyledTableCell
              align="center"
              sx={{
                fontWeight: 600,
                fontFamily: "Montserrat",
                fontSize: "16px",
                lineHeight: "19.5px",
                color: "#FFFFFF",
              }}
            >
              Course Status
            </StyledTableCell>
            <StyledTableCell
              align="center"
              sx={{
                fontWeight: 600,
                fontFamily: "Montserrat",
                fontSize: "16px",
                lineHeight: "19.5px",
                color: "#FFFFFF",
              }}
            >
              Action
            </StyledTableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {courses.map((row, index) => {
            let status = "";
            if (row.isActive) {
              status = "Active";
            } else {
              status = "Inactive";
            }
            return (
              <StyledTableRow key={row.courseId}>
                <StyledTableCell
                  component="th"
                  scope="row"
                  sx={{
                    fontWeight: 500,
                    fontFamily: "Montserrat",
                    fontSize: "16px",
                    lineHeight: "19.5px",
                    color: "#4F4F4F",
                  }}
                >
                  {index + 1}
                </StyledTableCell>
                <StyledTableCell
                  align="center"
                  sx={{
                    fontWeight: 500,
                    fontFamily: "Montserrat",
                    fontSize: "16px",
                    lineHeight: "19.5px",
                    color: "#4F4F4F",
                  }}
                >
                  {row.courseName}
                </StyledTableCell>
                <StyledTableCell
                  align="center"
                  sx={{
                    fontWeight: 500,
                    fontFamily: "Montserrat",
                    fontSize: "16px",
                    lineHeight: "19.5px",
                    color: "#4F4F4F",
                  }}
                >
                  {row.courseDetail}
                </StyledTableCell>
                <StyledTableCell
                  align="center"
                  sx={{
                    fontWeight: 500,
                    fontFamily: "Montserrat",
                    fontSize: "16px",
                    lineHeight: "19.5px",
                    color: "#4F4F4F",
                  }}
                >
                  {row.category.categoryName}
                </StyledTableCell>
                <StyledTableCell
                  align="center"
                  sx={{
                    fontWeight: 500,
                    fontFamily: "Montserrat",
                    fontSize: "16px",
                    lineHeight: "19.5px",
                    color: "#4F4F4F",
                  }}
                >
                  {row.coursePrice}
                </StyledTableCell>
                <StyledTableCell align="center">
                  <img
                    src={`${import.meta.env.VITE_API_URL}/${row.courseImage}`}
                    alt=""
                    style={{ width: "40px", height: "40px" }}
                  />
                </StyledTableCell>
                <StyledTableCell
                  align="center"
                  sx={{
                    fontWeight: 500,
                    fontFamily: "Montserrat",
                    fontSize: "16px",
                    lineHeight: "19.5px",
                    color: "#4F4F4F",
                  }}
                >
                  {status}
                </StyledTableCell>
                <StyledTableCell align="center">
                  <Button
                    sx={{
                      padding: "10px",
                      background: "#FABC1D",
                      color: "#5B4947",
                      borderRadius: "8px",
                      width: "180px",
                      height: "37px",
                      fontWeight: 700,
                      fontFamily: "Montserrat",
                      fontSize: "14px",
                      lineHeight: "17.07px",
                      textTransform: "capitalize",
                    }}
                    onClick={() => {
                      setSelectedCourse(row);
                      setIsDialogOpen(true);
                    }}
                  >
                    Edit
                  </Button>
                </StyledTableCell>
              </StyledTableRow>
            );
          })}
        </TableBody>
      </Table>
      {selectedCourse && (
        <Dialog open={isDialogOpen} onClose={() => setIsDialogOpen(false)}>
          <DialogTitle>Edit Course</DialogTitle>
          <DialogContent>
            <Box display="flex" flexDirection="column">
              <TextField
                label="Course Name"
                value={selectedCourse.courseName}
                onChange={handleInputChange}
                name="courseName"
                sx={{ marginTop: "10px" }}
              />
              <TextField
                label="Course Detail"
                value={selectedCourse.courseDetail}
                onChange={handleInputChange}
                name="courseDetail"
                sx={{ marginTop: "10px" }}
              />
              <TextField
                label="Course Price"
                value={selectedCourse.coursePrice}
                onChange={handleInputChange}
                name="coursePrice"
                sx={{ marginTop: "10px" }}
              />
              <FormControl fullWidth>
                <InputLabel id="demo-simple-select-label">Category</InputLabel>
                <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  value={selectedCourse.courseCategory}
                  label="Category"
                  onChange={(e) =>
                    setSelectedCourse({
                      ...selectedCourse,
                      courseCategory: e.target.value,
                    })
                  }
                >
                  {data.map((row) => (
                    <MenuItem key={row.idCategory} value={row.idCategory}>
                      {row.categoryName}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>
              <div>
                <Typography id="modal-modal-description" sx={{ mt: 1 }}>
                  Image
                </Typography>
                <div style={{ marginTop: "5px" }}>
                  <TextField
                    type="file"
                    size="small"
                    variant="outlined"
                    onChange={(e) =>
                      setSelectedCourse({
                        ...selectedCourse,
                        courseImage: e.target.files[0],
                      })
                    }
                  />
                </div>
              </div>
              <div>
                <Typography id="modal-modal-description" sx={{ mt: 1 }}>
                  Status
                </Typography>
                <div style={{ marginTop: "5px" }}>
                  <RadioGroup
                    row
                    aria-labelledby="demo-row-radio-buttons-group-label"
                    name="courseStatus" // Field name in selectedUser object
                    value={selectedCourse.courseStatus} // Convert to string for RadioGroup
                    onChange={handleInputChange}
                  >
                    <FormControlLabel
                      value={true}
                      control={<Radio />}
                      label="Active"
                    />
                    <FormControlLabel
                      value={false}
                      control={<Radio />}
                      label="Inactive"
                    />
                  </RadioGroup>
                </div>
              </div>
            </Box>
          </DialogContent>
          <DialogActions>
            <Button onClick={() => setIsDialogOpen(false)}>Cancel</Button>
            <Button onClick={handleSave}>Save</Button>
          </DialogActions>
        </Dialog>
      )}
    </TableContainer>
  );
}
