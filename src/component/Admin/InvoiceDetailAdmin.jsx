/* eslint-disable no-unused-vars */
import React from "react";
import {
  Box,
  Stack,
  Table,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
  styled,
  tableCellClasses,
} from "@mui/material";
// import InvoiceDetailsTable from '../../components/InvoiceDetailsTable';
import { useParams, Link } from "react-router-dom";
import { useEffect, useState } from "react";
import axios from "axios";
import MapInvoiceDetail from "../Invoice/MapInvoiceDetail";
import { mainColor } from "../Colors";
import ContainedButtonAdmin from "../Button/ContainedButtonAdmin";

const StyledTableCell = styled(TableCell)(() => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: "#5B4947",
    color: "white",
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));

const InvoiceDetailAdmin = () => {
  const { idInvoice } = useParams();
  const [invoiceDetails, setInvoiceDetail] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);

  useEffect(() => {
    const fetchData = async () => {
      try {
        setLoading(true);
        const response = await axios.get(
          `${
            import.meta.env.VITE_API_URL
          }/api/InvoiceDetails/InvoiceDetailAdmin/${idInvoice}`
        );
        setInvoiceDetail(response.data);
      } catch (error) {
        setError(error);
      } finally {
        setLoading(false);
      }
    };

    fetchData();
  }, []); // Empty dependency array to run the effect only once on mount

  if (loading) {
    return;
  }

  const invoice = invoiceDetails[0].invoice;
  const totalPrice = invoiceDetails.reduce((sum, data) => {
    return sum + data.course.coursePrice;
  }, 0);

  // console.log("array invoice details", invoiceDetails[0]);
  return (
    <Box>
      <Typography
        sx={{ fontWeight: "700", color: "#4F4F4F", marginBottom: "30px" }}
      >
        Details Invoice
      </Typography>

      <Stack direction={"row"} spacing={2} sx={{ marginY: "8" }}>
        <Typography>{"No Invoice : "}</Typography>
        <Typography>{invoice.idInvoice}</Typography>
      </Stack>
      <Stack direction={"row"} spacing={2} justifyContent="space-between">
        <Stack direction={"row"} spacing={8}>
          <Typography>Date : </Typography>
          <Typography>{invoice.formattedDate}</Typography>
        </Stack>
        <Typography>Total Price IDR {totalPrice.toLocaleString()}</Typography>
      </Stack>

      <TableContainer>
        <Table
          sx={{ minWidth: 650, marginTop: "24px" }}
          aria-label="simple table"
        >
          <TableHead sx={{ backgroundColor: mainColor }}>
            <TableRow>
              <StyledTableCell align="center">No</StyledTableCell>
              <StyledTableCell align="center">Course Name </StyledTableCell>
              <StyledTableCell align="center">Type</StyledTableCell>
              <StyledTableCell align="center">Schedule</StyledTableCell>
              <StyledTableCell align="center">Price</StyledTableCell>
            </TableRow>
          </TableHead>
          <MapInvoiceDetail invoiceDetails={invoiceDetails} />
        </Table>
      </TableContainer>
      <Link to="/dashboardadmin">
        <ContainedButtonAdmin
          title={"Pergi ke Halaman Dashboard"}
          marginTop={"20px"}
        ></ContainedButtonAdmin>
      </Link>
    </Box>
  );
};

export default InvoiceDetailAdmin;
