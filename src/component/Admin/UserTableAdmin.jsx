/* eslint-disable no-unused-vars */
import { useContext, useState, useEffect } from "react";
import { styled } from "@mui/material/styles";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell, { tableCellClasses } from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import Radio from "@mui/material/Radio";
import RadioGroup from "@mui/material/RadioGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import {
  Button,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  TextField,
  Box,
  Typography,
  FormControl,
} from "@mui/material";
import axios from "axios";
import { AuthContext } from "../../contexts/AuthContext";

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: "#5B4947",
    color: theme.palette.common.white,
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  "&:nth-of-type(even)": {
    backgroundColor: "rgba(91, 73, 71, 0.2)",
  },
  // hide last border
  "&:last-child td, &:last-child th": {
    border: 0,
  },
}));

// ... (imports and styled components remain the same)

export default function UserTableAdmin() {
  const [usersData, setUsersData] = useState([]);
  const [selectedUser, setSelectedUser] = useState(null);
  const [isDialogOpen, setIsDialogOpen] = useState(false);
  const [nameError, setNameError] = useState("");
  const [emailError, setEmailError] = useState("");
  const [roleError, setRoleError] = useState("");

  const { token } = useContext(AuthContext);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get(
          `${import.meta.env.VITE_API_URL}/api/Auth/AllUser`
        );
        setUsersData(response.data);
      } catch (error) {
        console.error("Error fetching user data:", error);
      }
    };
    fetchData();
  }, []);

  const handleInputChange = (e) => {
    const { name, type } = e.target;
    let value = e.target.value;

    // Validation error messages
    let nameError = "";
    let emailError = "";
    let roleError = "";

    if (type === "checkbox" || type === "boolean") {
      // Handle checkbox or boolean values
    } else if (type === "radio") {
      if (name === "role") {
        value = e.target.value;
        setRoleError(value.trim() === "" ? "Role is required" : "");
      } else if (name === "is_Active") {
        value = e.target.value === "true";
      }
    } else {
      if (name === "username") {
        // Validate username (not empty)
        nameError = value.trim() === "" ? "Username is required" : "";
      } else if (name === "email") {
        // Validate email format
        const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
        emailError = value.trim() === "" ? "Email is required" : "";
        emailError = !emailRegex.test(value.trim())
          ? "Invalid email format"
          : emailError;
      }
    }

    // Update state and error variables
    setSelectedUser((prevUser) => ({
      ...prevUser,
      [name]: value,
    }));

    setNameError(nameError);
    setEmailError(emailError);
    setRoleError(roleError);
  };

  const handleSave = async () => {
    try {
      const editUrl = `${
        import.meta.env.VITE_API_URL
      }/api/Auth/UpdateUserById?id=${selectedUser.idUser}`;

      // Validate before saving
      if (
        selectedUser.username.trim() !== "" &&
        selectedUser.email.trim() !== "" &&
        selectedUser.role.trim() !== "" &&
        roleError === ""
      ) {
        const response = await axios.patch(editUrl, selectedUser, {
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${token}`,
          },
        });

        // console.log("User edited successfully:", response.data);
        window.alert("User edited successfully!");
        window.location.reload();
        setIsDialogOpen(false); // Close the dialog after saving
      } else {
        // Handle validation errors
        setNameError(
          selectedUser.username.trim() === "" ? "Username is required" : ""
        );
        setEmailError(
          selectedUser.email.trim() === "" ? "Email is required" : ""
        );
        setRoleError(selectedUser.role.trim() === "" ? "Role is required" : "");
      }
    } catch (error) {
      console.error("Error editing user:", error);
    }
  };
  // console.log(selectedUser);
  return (
    <TableContainer component={Paper}>
      <Table sx={{ minWidth: 700 }} aria-label="customized table">
        <TableHead
          sx={{
            width: "1140px",
            height: "60px",
            padding: "20px, 20px, 20px, 0px",
            gap: "24px",
          }}
        >
          <TableRow>
            <StyledTableCell
              sx={{
                fontWeight: 600,
                fontFamily: "Montserrat",
                fontSize: "16px",
                lineHeight: "19.5px",
                color: "#FFFFFF",
              }}
            >
              No
            </StyledTableCell>
            <StyledTableCell
              align="center"
              sx={{
                fontWeight: 600,
                fontFamily: "Montserrat",
                fontSize: "16px",
                lineHeight: "19.5px",
                color: "#FFFFFF",
              }}
            >
              Nama
            </StyledTableCell>
            <StyledTableCell
              align="center"
              sx={{
                fontWeight: 600,
                fontFamily: "Montserrat",
                fontSize: "16px",
                lineHeight: "19.5px",
                color: "#FFFFFF",
              }}
            >
              Email
            </StyledTableCell>
            <StyledTableCell
              align="center"
              sx={{
                fontWeight: 600,
                fontFamily: "Montserrat",
                fontSize: "16px",
                lineHeight: "19.5px",
                color: "#FFFFFF",
              }}
            >
              Role
            </StyledTableCell>
            <StyledTableCell
              align="center"
              sx={{
                fontWeight: 600,
                fontFamily: "Montserrat",
                fontSize: "16px",
                lineHeight: "19.5px",
                color: "#FFFFFF",
              }}
            >
              Status
            </StyledTableCell>
            <StyledTableCell
              align="center"
              sx={{
                fontWeight: 600,
                fontFamily: "Montserrat",
                fontSize: "16px",
                lineHeight: "19.5px",
                color: "#FFFFFF",
              }}
            >
              Action
            </StyledTableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {usersData.map((row, index) => {
            let role = "";
            let status = "";

            if (row.role === "ADMIN") {
              role = "Admin";
            } else if (row.role == "USER") {
              role = "User";
            }

            if (row.is_Active) {
              status = "Active";
            } else {
              status = "Inactive";
            }

            return (
              <StyledTableRow key={row.idUser}>
                <StyledTableCell
                  component="th"
                  scope="row"
                  sx={{
                    fontWeight: 500,
                    fontFamily: "Montserrat",
                    fontSize: "16px",
                    lineHeight: "19.5px",
                    color: "#4F4F4F",
                  }}
                >
                  {index + 1}
                </StyledTableCell>
                <StyledTableCell
                  align="center"
                  sx={{
                    fontWeight: 500,
                    fontFamily: "Montserrat",
                    fontSize: "16px",
                    lineHeight: "19.5px",
                    color: "#4F4F4F",
                  }}
                >
                  {row.username}
                </StyledTableCell>
                <StyledTableCell
                  align="center"
                  sx={{
                    fontWeight: 500,
                    fontFamily: "Montserrat",
                    fontSize: "16px",
                    lineHeight: "19.5px",
                    color: "#4F4F4F",
                  }}
                >
                  {row.email}
                </StyledTableCell>
                <StyledTableCell
                  align="center"
                  sx={{
                    fontWeight: 500,
                    fontFamily: "Montserrat",
                    fontSize: "16px",
                    lineHeight: "19.5px",
                    color: "#4F4F4F",
                  }}
                >
                  {role}
                </StyledTableCell>
                <StyledTableCell
                  align="center"
                  sx={{
                    fontWeight: 500,
                    fontFamily: "Montserrat",
                    fontSize: "16px",
                    lineHeight: "19.5px",
                    color: "#4F4F4F",
                  }}
                >
                  {status}
                </StyledTableCell>
                <StyledTableCell align="center">
                  <Button
                    sx={{
                      padding: "10px",
                      background: "#FABC1D",
                      color: "#5B4947",
                      borderRadius: "8px",
                      width: "180px",
                      height: "37px",
                      fontWeight: 700,
                      fontFamily: "Montserrat",
                      fontSize: "14px",
                      lineHeight: "17.07px",
                      textTransform: "capitalize",
                    }}
                    onClick={() => {
                      setSelectedUser(row);
                      setIsDialogOpen(true);
                    }}
                  >
                    Edit
                  </Button>
                </StyledTableCell>
              </StyledTableRow>
            );
          })}
        </TableBody>
      </Table>
      {selectedUser && (
        <Dialog open={isDialogOpen} onClose={() => setIsDialogOpen(false)}>
          <FormControl onSubmit={handleSave}>
            <DialogTitle>Edit User</DialogTitle>
            <DialogContent>
              <Box display="flex" flexDirection="column">
                <TextField
                  label="Username"
                  error={nameError !== ""}
                  value={selectedUser.username}
                  onChange={handleInputChange}
                  helperText={nameError}
                  name="username"
                  sx={{ marginTop: "10px" }}
                />
                <TextField
                  label="Email"
                  error={emailError !== ""}
                  value={selectedUser.email}
                  onChange={handleInputChange}
                  helperText={emailError}
                  name="email"
                  sx={{ marginTop: "10px" }}
                />

                <div>
                  <FormControl component="fieldset">
                    <Typography id="modal-modal-description" sx={{ mt: 1 }}>
                      Role
                    </Typography>
                    <div style={{ marginTop: "5px" }}>
                      <RadioGroup
                        row
                        aria-labelledby="demo-row-radio-buttons-group-label"
                        name="role"
                        value={selectedUser.role}
                        onChange={handleInputChange}
                      >
                        <FormControlLabel
                          value={"ADMIN"}
                          control={<Radio />}
                          label="Admin"
                        />
                        <FormControlLabel
                          value={"USER"}
                          control={<Radio />}
                          label="User"
                        />
                      </RadioGroup>
                    </div>
                  </FormControl>
                </div>

                <div>
                  <Typography id="modal-modal-description" sx={{ mt: 1 }}>
                    Status
                  </Typography>
                  <div style={{ marginTop: "5px" }}>
                    <RadioGroup
                      row
                      aria-labelledby="demo-row-radio-buttons-group-label"
                      name="is_Active" // Field name in selectedUser object
                      value={selectedUser.is_Active} // Convert to string for RadioGroup
                      onChange={handleInputChange}
                    >
                      <FormControlLabel
                        value={true}
                        control={<Radio />}
                        label="Active"
                      />
                      <FormControlLabel
                        value={false}
                        control={<Radio />}
                        label="Inactive"
                      />
                    </RadioGroup>
                  </div>
                </div>
              </Box>
            </DialogContent>
            <DialogActions>
              <Button onClick={() => setIsDialogOpen(false)}>Cancel</Button>
              <Button onClick={handleSave}>Save</Button>
            </DialogActions>
          </FormControl>
        </Dialog>
      )}
    </TableContainer>
  );
}
