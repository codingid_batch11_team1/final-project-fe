import { Button } from "@mui/material"
import { mainColor } from "../Colors"
import { PropTypes } from "prop-types"

export default function ButtonSuccess({title, onClick}) {

    return (
        <Button
            onClick={onClick}
            sx={{
                color: 'white',
                backgroundColor: mainColor(),
                padding: '15px 30px',
                borderRadius: '10px',
                "&:hover": {
                    color: "white",
                    backgroundColor: "#6a0008",
                },
            }}
        >
            {title}
        </Button>
    )
}

ButtonSuccess.propTypes = {
    title: PropTypes.string.isRequired,
    onClick: PropTypes.func,
}