import { Button } from "@mui/material"
import { mainColor } from "../Colors"
import { PropTypes } from "prop-types"

export default function ContainedButton({title, onClick}) {
    return (
        <Button
            variant='contained'
            type="submit"
            onClick={onClick}
            style={{
                backgroundColor: mainColor(), 
                padding: '7px 40px', 
                borderRadius: '10px',
            }}
        >
            {title}
        </Button>
    )
}

ContainedButton.propTypes = {
    onClick: PropTypes.func,
    title: PropTypes.string.isRequired,
};