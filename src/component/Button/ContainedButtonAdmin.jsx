import { Button } from "@mui/material";
import { PropTypes } from "prop-types";

export default function ContainedButtonAdmin({ title, onClick, marginTop }) {
  return (
    <Button
      variant="contained"
      type="submit"
      onClick={onClick}
      style={{
        backgroundColor: "#5B4947",
        padding: "7px 40px",
        borderRadius: "10px",
        marginTop: marginTop,
      }}
    >
      {title}
    </Button>
  );
}

ContainedButtonAdmin.propTypes = {
  onClick: PropTypes.func,
  title: PropTypes.string.isRequired,
  marginTop: PropTypes.string.isRequired,
};
