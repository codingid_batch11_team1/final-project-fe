import { Button } from "@mui/material"
import { mainColor } from "../Colors"
import { PropTypes } from "prop-types"

export default function ContainedButtonDetailClass({title, onClick, height}) {
    return (
        <Button
            variant='contained'
            type="submit"
            onClick={onClick}
            style={{
                backgroundColor: mainColor(), 
                minWidth: '200px', 
                borderRadius: '10px',
                height,
            }}
        >
            {title}
        </Button>
    )
}

ContainedButtonDetailClass.propTypes = {
    onClick: PropTypes.func,
    title: PropTypes.string.isRequired,
    height: PropTypes.string,
};