import { Button } from "@mui/material"
import {mainColor} from "../Colors"
import { PropTypes } from "prop-types"

export default function OutlinedButton({title, onClick, fontWeight}) {
    return (
        <Button
            variant='outlined'
            onClick={onClick}
            style={{
                borderColor: mainColor(),
                color: mainColor(), 
                padding: '7px 40px', 
                borderRadius: '10px',
                marginRight: "20px",
                fontWeight: fontWeight,
            }}
        >
            {title}
        </Button>
    )
}

OutlinedButton.propTypes = {
    onClick: PropTypes.func,
    title: PropTypes.string.isRequired,
    marginRight: PropTypes.string,
    fontWeight: PropTypes.string,
};