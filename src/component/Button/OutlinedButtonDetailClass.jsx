import { Button } from "@mui/material"
import { mainColor } from "../Colors"
import { PropTypes } from "prop-types"

export default function OutlinedButtonDetailClass({title, onClick, disabled, height}) {
    return (
        <Button
            variant='outlined'
            type="submit"
            onClick={onClick}
            disabled={disabled}
            style={{
                borderColor: mainColor(),
                color: mainColor(),
                minWidth: '200px', 
                borderRadius: '10px',
                height,
            }}
        >
            {title}
        </Button>
    )
}

OutlinedButtonDetailClass.propTypes = {
    onClick: PropTypes.func,
    disabled: PropTypes.bool,
    title: PropTypes.string.isRequired,
    height: PropTypes.string,
};