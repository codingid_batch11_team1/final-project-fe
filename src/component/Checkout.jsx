import { Box, Checkbox, Container, Typography } from "@mui/material";
import { PropTypes } from "prop-types"

export default function Checkout({ selectAll, onSelectAllChange }) {
  return (
    <Container maxWidth="lg">
      <Box
        sx={{
          marginTop: "80px",
          display: "flex",
          alignItems: "center",
          flexDirection: "row",
        }}
      >
        <Checkbox checked={selectAll} onChange={onSelectAllChange} />
        <Typography sx={{ marginLeft: "20px" }}>Pilih Semua</Typography>
      </Box>
    </Container>
  );
}

Checkout.propTypes = {
  selectAll: PropTypes.bool.isRequired,
  onSelectAllChange: PropTypes.func.isRequired
};