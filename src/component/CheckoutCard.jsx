import {
  Box,
  Checkbox,
  Container,
  Divider,
  IconButton,
  Stack,
  Typography,
} from "@mui/material";
import DeleteForeverIcon from "@mui/icons-material/DeleteForever";
import { mainColor } from "./Colors";
import React from "react";
import { PropTypes } from "prop-types"

export default function CheckoutCard({ checkouts, selectedCheckboxes, onCheckboxChange, selectAll, onDeleteCheckout }) {

  return (
    <Container maxWidth="lg">
      <Divider />
      <Stack
        direction="column" // This is the key change to stack items vertically
        spacing={2} // This adds space between each child component
        sx={{
          marginTop: "24px",
          width: "100%"
        }}
      >
        {checkouts.map((checkout) => (
          <React.Fragment key={checkout.idCheckout}>
            <Box
              sx={{
                width: "100%",
                display: "flex",
                alignItems: "center",
                justifyContent: "space-between"
              }}
            >
              <Box
                sx={{
                  display: 'flex',
                  alignItems: 'center',
                  flexGrow: 1, // allows this box to grow and take up space
                }}
              >
                <Checkbox
                  checked={selectAll || selectedCheckboxes[checkout.idCheckout] || false}
                  onChange={(e) => onCheckboxChange(checkout.idCheckout, e.target.checked)}
                />
                <Box
                  ml={2}
                  component="img"
                  sx={{
                    width: { xs: 100, md: 150, lg: 200 },
                  }}
                  alt={checkout.course.courseName}
                  src={`${import.meta.env.VITE_API_URL}${checkout.course.courseImage}`}
                />
                <Stack
                  direction="column"
                  mx={4}
                  sx={{ marginY: { xs: "20px", md: "10px", lg: "5px" } }}
                >
                  <Typography>{checkout.category.categoryName}</Typography>
                  <Typography
                    fontWeight="600"
                    fontSize={{ xs: "16px", md: "20px", lg: "24px" }}
                  >
                    {checkout.course.courseName}
                  </Typography>
                  <Typography fontSize={{ xs: "12px", md: "14px", lg: "16px" }}>
                    Schedule : {checkout.schedule.formattedDate}
                  </Typography>
                  <Typography
                    color={mainColor()}
                    fontWeight="600"
                    fontSize={{ xs: "12px", md: "16px", lg: "20px" }}
                  >
                    IDR {checkout.course.coursePrice}
                  </Typography>
                </Stack>
              </Box>
              <IconButton onClick={() => onDeleteCheckout(checkout.idCheckout)}>
                <DeleteForeverIcon
                  sx={{
                    width: "40px",
                    height: "40px",
                    color: "#EB5757",
                  }} />
              </IconButton>
            </Box>
            <Divider />
          </React.Fragment>
        ))}
      </Stack>
    </Container>
  );
}

CheckoutCard.propTypes = {
  checkouts: PropTypes.arrayOf(
    PropTypes.shape({
      idCheckout: PropTypes.number.isRequired,
      course: PropTypes.shape({
        courseName: PropTypes.string.isRequired,
        coursePrice: PropTypes.number.isRequired,
        courseImage: PropTypes.string.isRequired
      }).isRequired,
      category: PropTypes.shape({
        categoryName: PropTypes.string.isRequired
      }).isRequired,
      schedule: PropTypes.shape({
        formattedDate: PropTypes.string.isRequired
      }).isRequired
    })
  ).isRequired,
  selectedCheckboxes: PropTypes.objectOf(PropTypes.bool).isRequired,
  onCheckboxChange: PropTypes.func.isRequired,
  selectAll: PropTypes.bool.isRequired,
  onDeleteCheckout: PropTypes.func.isRequired
};
