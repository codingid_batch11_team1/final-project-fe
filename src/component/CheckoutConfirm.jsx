import { Box, Button, Paper, Stack, Typography } from "@mui/material";
import { mainColor } from "./Colors";
import { useState, useEffect } from "react";

export default function CheckoutConfirm({ checkouts, selectedCheckboxes, onOpenPaymentModal }) {
  const [totalPrice, setTotalPrice] = useState(0);

  useEffect(() => {
    const calculateTotalPrice = () => {
      if (!Array.isArray(checkouts)) {
        console.error('Checkouts is not an array');
        return 0;
      }
      const total = checkouts.reduce((total, checkout) => {
        const isSelected = selectedCheckboxes[checkout.idCheckout];
        if (isSelected) {
          return total + checkout.course.coursePrice;
        }
        return total;
      }, 0);
      return total;
    };

    setTotalPrice(calculateTotalPrice());
  }, [checkouts, selectedCheckboxes]);

  return (
    <Box>
      <Paper
        sx={{
          display: "flex",
          flexDirection: { xs: "column", md: "row", lg: "row" },
          justifyContent: {
            xs: "center",
            md: "space-between",
            lg: "space-between",
          },
          alignItems: "center",
          position: "fixed",
          bottom: 0,
          left: 0,
          right: 0,
          padding: { xs: "10px 10px", md: "30px 70px", lg: "30px 70px" },
        }}
        elevation={3}
      >
        <Stack
          spacing={2}
          display={{ xs: "none", md: "flex", lg: "flex" }}
          direction={{ xs: "column", md: "row", lg: "row" }}
          alignItems="center"
          sx={{ marginBottom: { xs: "10px", lg: "0" } }}
        >
          <Typography> Total Price </Typography>
          <Typography fontSize="24px" fontWeight="600" color="#790B0A">
            IDR {totalPrice}
          </Typography>
        </Stack>
        <Button
          variant="contained"
          onClick={onOpenPaymentModal}
          sx={{ backgroundColor: mainColor() }}
        >
          Pay Now
        </Button>
      </Paper>
    </Box>
  );
}
