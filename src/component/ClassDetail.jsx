/* eslint-disable no-unused-vars */
import { Typography, Grid, Stack, Container, Button, useTheme, useMediaQuery, Box } from "@mui/material";
import { useState, useEffect, useContext } from "react";
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import { mainColor } from "./Colors"
import { useParams } from "react-router-dom";
import useGetScheduleByCourseId from "./Hooks/useGetScheduleByCourseId";
import ContainedButtonDetailClass from "./Button/ContainedButtonDetailClass";
import OutlinedButtonDetailClass from "./Button/OutlinedButtonDetailClass";
import useAddToCheckout from "./Hooks/useAddToCheckout";
import PaymentModal from "./modal/PaymentModal";
import { AuthContext } from "../contexts/AuthContext";

export default function ClassDetail() {
    const [schedule, setSchedule] = useState('');
    const [isPaymentModalOpen, setIsPaymentModalOpen] = useState(false);
    const { idCourse } = useParams();
    const { token } = useContext(AuthContext)
    const { schedules, course, getScheduleByIdLoading, error } = useGetScheduleByCourseId(idCourse);
    const { addToCheckout, isLoading } = useAddToCheckout();
      
      // Now, schedules array has each schedule with a formattedDate property.
      

    const theme = useTheme();
    const isDesktop = useMediaQuery(theme.breakpoints.up('lg'));
    const isDesktopxl = useMediaQuery(theme.breakpoints.up('xl'));

    useEffect(() => {
        // Reset the schedule state to null whenever the idCourse changes
        setSchedule('');
    }, [idCourse]); // Only re-run the effect if idCourse changes

    if (getScheduleByIdLoading || !course) {
        return <div>Loading...</div>;
    }

    const handleAddToCart = async () => {
        await addToCheckout({ idSchedule: schedule });
    };

    const handleBuyNow = () => {
        if (!token) {
            return alert("You need to login first")
        } else if (!schedule) {
            return alert("You have not pick a schedule yet.")
        }
        
        setIsPaymentModalOpen(true); // This will open the payment modal
    };

    return (
        <Box>
            <Container
                maxWidth="md"
                sx={{
                    mt: 15,
                    ml: isDesktopxl ? 44 : isDesktop ? 5 : 'auto',
                    marginRight: isDesktop ? 'auto' : 'auto' // Keeping right margin 'auto' to ensure centered alignment in all cases.
                }}
            >
                <Grid container spacing={4} justifyContent='center' alignItems='center'>
                    <Grid item xs={12} md={6}>
                        <img
                            src={`${import.meta.env.VITE_API_URL}/${course.courseImage}`}
                            style={{ width: '100%', aspectRatio: '16/9' }}
                            alt={course.courseName} />
                    </Grid>
                    <Grid item xs={12} md={6}>
                        <Stack spacing={3} justifyContent='flex-start'>
                            <Typography variant="body1">
                                {course.category?.categoryName}
                            </Typography>
                            <Typography fontWeight={700} variant="h4">
                                {course.courseName}
                            </Typography>
                            <Typography variant="h5" color={mainColor()}>
                                IDR {course.coursePrice}
                            </Typography>
                            <FormControl
                                variant="outlined"
                                size="small"
                                sx={{
                                    minWidth: '60%',
                                    maxWidth: isDesktop ? '70%' : '100%',
                                    marginBottom: '20px'
                                }}
                            >
                                <InputLabel>Select Schedule</InputLabel>
                                <Select
                                    label="Select Schedule"
                                    value={schedule}
                                    onChange={e => setSchedule(e.target.value)}
                                >
                                    {schedules.map((schedule) => (
                                        <MenuItem key={schedule.idSchedule} value={schedule.idSchedule}>
                                            {schedule.formattedDate}
                                        </MenuItem>
                                    ))}
                                </Select>
                            </FormControl>
                            <Grid
                                container
                                spacing={2}
                            >
                                <Grid item xs={12} sm={6}>
                                    <OutlinedButtonDetailClass
                                        variant="outlined"
                                        title="Add to Cart"
                                        onClick={handleAddToCart}
                                        disabled={isLoading}
                                    >
                                    </OutlinedButtonDetailClass>
                                </Grid>
                                <Grid item xs={12} sm={6}>
                                    <ContainedButtonDetailClass
                                        title="Buy Now"
                                        onClick={handleBuyNow}
                                    >
                                    </ContainedButtonDetailClass>
                                </Grid>
                            </Grid>
                        </Stack>
                    </Grid>
                </Grid>

            </Container>
            <Container maxWidth='lg' sx={{ mt: 7 }}>
                <Typography gutterBottom variant="h5" component="div">
                    {course.category.categoryName}
                </Typography>
                <Typography variant="body2" color="text.secondary" mb={3}>
                    {course.courseDetail}
                </Typography>
            </Container>
            <PaymentModal
                open={isPaymentModalOpen}
                handleClose={() => setIsPaymentModalOpen(false)}
                schedule={schedule}
            />
        </Box>
    );
}
