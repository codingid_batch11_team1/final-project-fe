import { Typography, Grid, Box } from "@mui/material";
import GambarEmailSuccess from "../assets/GambarEmailConfirmSuccess.png";
import { Link } from "react-router-dom";
import ButtonSuccess from "./Button/ButtonSuccess";
import useEmailConfirm from "./Hooks/useEmailConfirmation";

function EmailConfirmSuccess() {
  const queryParameters = new URLSearchParams(window.location.search);
  const email = queryParameters.get("email");
  const { emailConfirmationFunc } = useEmailConfirm();
  // console.log(email);

  emailConfirmationFunc(email);

  return (
    <Box
      display="flex"
      alignItems="center"
      justifyContent="center"
      style={{ height: "100vh" }}
    >
      <Grid container direction="column" spacing={5} alignItems="center">
        <Grid item>
          <img
            src={GambarEmailSuccess}
            alt="Email Confirm Success"
            style={{ maxWidth: "200px", height: "auto" }}
          />
        </Grid>

        <Grid item>
          <Typography variant="h4" align="center" color={"#790B0A"}>
            Email Confirmation Success
          </Typography>
        </Grid>

        <Grid item>
          <Typography align="center" variant="h6" mt={-4}>
            Your email already! Please login first to access the web
          </Typography>
        </Grid>

        <Grid item>
          <Link to={"/login"}>
            <ButtonSuccess title="Login" onClick="" />
          </Link>
        </Grid>
      </Grid>
    </Box>
  );
}

export default EmailConfirmSuccess;
