import {
  Box,
  Container,
  Grid,
  Typography,
  List,
  ListItem,
} from "@mui/material";
import Stack from "@mui/material/Stack";
import { Link } from "react-router-dom";
import useGetCategory from "./Hooks/useGetCategory";

export default function Footer() {
  const { data: products } = useGetCategory();

  const midpoint = Math.ceil(products.length / 2);

  const productsArr1 = products.slice(0, midpoint).map((item) => item);
  const productsArr2 = products.slice(midpoint).map((item) => item);

  return (
    <Box
      display="flex"
      flexDirection="column"
      alignItems="start"
      justifyContent="center"
      mt={3}
      mb={5}
    >
      <Container maxWidth="lg">
        <Grid container spacing={5}>
          <Grid item xs={12} md={4}>
            <Typography variant="h5" gutterBottom>
              About Us
            </Typography>
            <Typography>
              Sed ut perspiciatis unde omnis iste natus error sit voluptatem
              accusantium doloremque laudantium, totam rem aperiam, eaque ipsa
              quae ab illo inventore veritatis et quasi architecto beatae vitae
              dicta sunt explicabo.{" "}
            </Typography>
          </Grid>
          <Grid item xs={12} md={4}>
            <Typography variant="h5">Product</Typography>
            <Grid container>
              <Grid item xs={6}>
                <List>
                  {productsArr1.map((product) => (
                    <ListItem key={product.idCategory}>
                      <Link
                        to={`/landing_list_menu_class_page/${product.idCategory}`}
                        style={{ textDecoration: "none", color: "black" }}
                      >
                        • {product.categoryName}
                      </Link>
                    </ListItem>
                  ))}
                </List>
              </Grid>
              <Grid item xs={6}>
                <List>
                  {productsArr2.map((product) => (
                    <ListItem key={product.idCategory}>
                      <Link
                        to={`/landing_list_menu_class_page/${product.idCategory}`}
                        style={{ textDecoration: "none", color: "black" }}
                        key={product.idCategory}
                      >
                        • {product.categoryName}
                      </Link>
                    </ListItem>
                  ))}
                </List>
              </Grid>
            </Grid>
          </Grid>
          <Grid item xs={12} md={4}>
            <Typography variant="h5" gutterBottom>
              Address
            </Typography>
            <Typography mb={2}>
              Sed ut perspiciatis unde omnis iste natus error sit voluptatem
              accusantium doloremque.
            </Typography>
            <Typography variant="h5" gutterBottom>
              Contact Us
            </Typography>
            <Stack flexDirection="row" gap={2}>
              <img src="/icon/phone.png" width="40px" />
              <img src="/icon/instagram.png" width="40px" />
              <img src="/icon/youtube.png" width="40px" />
              <img src="/icon/telegram.png" width="40px" />
              <img src="/icon/message.png" width="40px" />
            </Stack>
          </Grid>
        </Grid>
      </Container>
    </Box>
  );
}
