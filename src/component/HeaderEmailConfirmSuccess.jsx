import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Typography';
import Logo from '/logo.png'
import { Link } from 'react-router-dom';

function HeaderEmailConfirmSuccess() {
  return (
    <AppBar component="nav" sx={{ backgroundColor: 'white', color: 'black', boxShadow: 'none', height: '100px' }}>
      <Box
        component="div"
        ml={3}
        mt={1}
        sx={{
          flexGrow: 1,
          display: { xs: 'none', sm: 'flex' },
        }}>
        <Link to={'/'}>
          <img src={Logo} alt="Otomobil Logo" height={50} style={{ verticalAlign: 'middle' }} />
        </Link>
      </Box>
    </AppBar>
  );
}

export default HeaderEmailConfirmSuccess;
