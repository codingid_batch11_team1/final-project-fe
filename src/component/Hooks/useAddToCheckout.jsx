import { useState, useContext } from 'react';
import axios from 'axios';
import { AuthContext } from "../../contexts/AuthContext";

const useAddToCheckout = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(null);
  const { token } = useContext(AuthContext);

  const handleError = (err) => {
    if (err.response) {
      let errorMessage;
      switch (err.response.status) {
        case 400:
          errorMessage = err.response.data || "Bad request";
          break;
        case 401:
          errorMessage = err.response.data || "You need to login first";
          break;
        default:
          errorMessage = "An unexpected error occurred";
      }
      return errorMessage;
    }
  };

  const addToCheckout = async ({ idSchedule }) => {
    setIsLoading(true);
    setError(null);

    const postData = {
      fkIdSchedule: idSchedule ? idSchedule : 0,
    };

    try {
      const res = await axios.post(`${import.meta.env.VITE_API_URL}/api/Checkout`, postData, {
        headers: {
          "Content-Type": "application/json",
          "Authorization": `Bearer ${token}`,
        },
      });
      setIsLoading(false);
      alert("Added to checkout");
    } catch (err) {
      setIsLoading(false);
      const errorMessage = handleError(err);
      setError(errorMessage);
      alert(errorMessage);
      // Optionally, use a more integrated approach to display error messages
    }
  };

  return {
    addToCheckout,
    isLoading,
    error,
  };
};

export default useAddToCheckout;
