import { useContext, useState } from "react";
import axios from "axios";
import { AuthContext } from "../../contexts/AuthContext";

const useAddUserByAdmin = () => {
  const [response, setResponse] = useState(null);
  const [userName, setUserName] = useState('');
  const [password, setPassword] = useState('');
  const [email, setEmail] = useState('');
  const [role, setRole] = useState("");
  const [status, setStatus] = useState(true);
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);
  const { token } = useContext(AuthContext);

  const submitUser = async () => {
    try {
      setLoading(true);
      const response = await axios.post(
        `${import.meta.env.VITE_API_URL}/api/Auth/CreateUserByAdmin`,
        {
          username: userName,
          password: password,
          email: email,
          role: role,
          isActive: status
        },
        {
          headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${token}`, // Use the token here
          },
        });
      setResponse(response.data);
    } catch (error) {
      setError(error);
    } finally {
      setLoading(false);
    }
  };

  return { response, error, loading, submitUser, userName, password, email, setUserName, setPassword, setEmail, setRole, setStatus };
};

export default useAddUserByAdmin;
