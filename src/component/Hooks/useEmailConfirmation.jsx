import { useState } from "react";
import axios from "axios";

const useEmailConfirm = () => {
  const [errorMessage, setErrorMessage] = useState("");
  const emailConfirmationFunc = (email) => {
    axios
      .patch(
        `${import.meta.env.VITE_API_URL}/api/Auth/isActive`, // Assuming this is the correct endpoint
        {
          email: email,
        },
        {
          headers: {
            "Content-Type": "application/json",
          },
        }
      )
      .then((response) => {
        // Handle the response here, you can check if the response status is 200 (OK) to ensure the email confirmation was successful
        if (response.status === 200) {
          alert("Email confirmation successful. Please click the button to go to the login page.");
        } else {
          setErrorMessage("Email confirmation failed.");
        }
      })
      .catch((err) => {
        setErrorMessage(err.response.data);
      });
  };

  return {
    emailConfirmationFunc,
    setErrorMessage,
    errorMessage,
  };
};

export default useEmailConfirm;
