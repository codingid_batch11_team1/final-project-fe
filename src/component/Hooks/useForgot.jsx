import { useState } from "react";
import { useNavigate } from "react-router-dom";
import axios from "axios";

const useForgot = () => {
  const navigate = useNavigate();
  const [email, setEmail] = useState("");
  const [errorMessage, setErrorMessage] = useState("");
  const [emailErrorMessage, setEmailErrorMessage] = useState();

  // Validate the email
  const validateEmail = () => {
    const isEmailValid = /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email);
    if (!email) {
      setEmailErrorMessage("Email is required");
    } else if (!isEmailValid) {
      setEmailErrorMessage("Email must be valid");
    } else {
      setEmailErrorMessage("");
    }
    return isEmailValid;
  };

  const forgotButtonFunc = () => {
    if (!validateEmail()) {
      // Do not proceed if email is not valid
      return;
    }

    axios
      .post(
        `${import.meta.env.VITE_API_URL}/api/Auth/ForgotPassword`,
        {
          email: email,
        },
        {
          headers: {
            "Content-Type": "application/json",
          },
        }
      )
      .then(() => {
        alert("Please check your email/Silahkan cek email"); // "Please check your email" in Indonesian
        setTimeout(() => {
          navigate("/");
        }, 2000);
      })
      .catch((err) => {
        if (err.response.status === 404) {
          // Email not found in Database
          setErrorMessage("Email not found");
          alert("Email not found");
        } else {
          // Other errors
          setErrorMessage("Please input the correct email");
          alert("Please input the correct email");
        }
      });
  };
  return {
    forgotButtonFunc,
    setEmail,
    errorMessage,
    email,
    emailErrorMessage,
  };
};

export default useForgot;
