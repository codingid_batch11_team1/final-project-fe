import axios from "axios";
import { useState } from "react";

export default function useGetByIdCategory() {
  // const [getScheduleByIdLoading, setGetScheduleByIdLoading] = useState(false);
  const [category, setCategory] = useState(null);

  const getByIdCategory = async (id) => {
    // const res = await axios.get(`${import.meta.env.VITE_API_URL}/product`);
    const res = await axios.get(
      `${import.meta.env.VITE_API_URL}/api/CourseCategory/${id}`
    );
    const data = await res.data;

    // console.log(id);
    // console.log(data);
    setCategory(data);
  };

  return {
    getByIdCategory,
    category,
  };
}
