import { useState, useEffect, useContext } from "react";
import axios from "axios";
import { AuthContext } from "../../contexts/AuthContext";
import { useParams } from "react-router-dom";

const useGetCategory = () => {
  const [data, setData] = useState([]);
  const [error, setError] = useState(null);

  const { role } = useContext(AuthContext);

  const { idCategory, idCourse } = useParams();

  const categoryParams = {
    limit: role === "ADMIN" ? null : 6, // Set limit based on the role
    ...(idCategory ? { categoryId: idCategory } : {}), // Conditionally add categoryId if it exists
    ...(idCourse ? { courseId: idCourse } : {}), // Conditionally add courseId if it exists
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get(
          `${import.meta.env.VITE_API_URL}/api/CourseCategory/admin`,
          { params: categoryParams }, {
            headers: {
              "Content-Type": "application/json",
            },
          }
        );
        setData(response.data);
      } catch (error) {
        setError(error);
      }
    };

    fetchData();
  }, []);

  return { data, error };
};

export default useGetCategory;
