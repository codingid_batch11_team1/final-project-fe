import { useState, useEffect } from "react";
import axios from "axios";

const useGetCategoryById = (idCategory) => {
  const [data, setData] = useState([]);
  const [error, setError] = useState(null);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    const fetchData = async () => {
      setIsLoading(true);
      try {
        const response = await axios.get(
          `${import.meta.env.VITE_API_URL}/api/CourseCategory/${idCategory}`
        );
        setData(response.data);
      } catch (error) {
        setError(error);
      }
      setIsLoading(false);
    };
  
    fetchData();
  }, [idCategory]);
  
  return { data, error, isLoading };
  
};

export default useGetCategoryById;
