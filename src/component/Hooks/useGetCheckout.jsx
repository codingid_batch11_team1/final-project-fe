import { useState, useContext, useEffect } from 'react';
import axios from 'axios';
import { AuthContext } from "../../contexts/AuthContext";

const useGetCheckout = () => {
    const [isLoading, setIsLoading] = useState(false);
    const [error, setError] = useState(null);
    const [checkouts, setCheckouts] = useState([]);
    const { token } = useContext(AuthContext);

    // Define getCheckout outside of useEffect so it's in the scope of the entire hook
    const getCheckout = async () => {
        setIsLoading(true);
        setError(null);

        try {
            const res = await axios.get(`${import.meta.env.VITE_API_URL}/api/Checkout`, {
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": `Bearer ${token}`,
                },
            });
            setCheckouts(res.data);
        } catch (err) {
            setError(err);
        } finally {
            setIsLoading(false);
        }
    };

    // Use useEffect to call getCheckout on mount and when token changes
    useEffect(() => {
        getCheckout();
    }, [token]); // Add getCheckout to dependencies array

    return {
        checkouts,
        isLoading,
        error,
        getCheckout, // Now getCheckout is accessible here
    };
};

export default useGetCheckout;
