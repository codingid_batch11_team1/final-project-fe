/* eslint-disable react-hooks/exhaustive-deps */
import { useContext, useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import axios from "axios";
import { AuthContext } from "../../contexts/AuthContext";

export default function useGetCourseCard() {
  const [loading, setLoading] = useState(true);
  const [courses, setCourses] = useState([]);
  const [error, setError] = useState(null);

  const { role } = useContext(AuthContext);

  // Use `useParams` if `idCategory` and `courseId` are URL parameters
  const { idCategory, idCourse } = useParams();

  // Define your parameters based on conditions or component state
  const courseParams = {
    limit: role === "ADMIN" ? null : 6, // Set limit based on the role
    ...(idCategory ? { categoryId: idCategory } : {}), // Conditionally add categoryId if it exists
    ...(idCourse ? { courseId: idCourse } : {}), // Conditionally add courseId if it exists
  };

  useEffect(() => {
    async function getCourseCard() {
      try {
        setLoading(true);
        const response = await axios.get(
          `${import.meta.env.VITE_API_URL}/api/Course`,
          { params: courseParams }
        );
        setCourses(response.data);
      } catch (error) {
        setError(error);
      } finally {
        setLoading(false);
      }
    }

    getCourseCard();
  }, [idCategory, idCourse]); // Now it depends on idCategory and idCourse

  return { loading, courses, error };
}
