/* eslint-disable no-unused-vars */
/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import axios from "axios";

export default function useGetCourseCard() {
  const [loading, setLoading] = useState(true);
  const [invoice, setInvoice] = useState([]);
  const [error, setError] = useState(null);
  
  
  useEffect(() => {
    async function getInvoice() {
      try {
        setLoading(true);
        const response = await axios.get(`${import.meta.env.VITE_API_URL}/api/Invoice`);
        setInvoice(response.data);
      } catch (error) {
        setError(error);
      } finally {
        setLoading(false);
      }
    }

    getInvoice();
  }, []); // Now it depends on idCategory and idCourse

  return { loading, invoice, error };
}
