import { useEffect, useState, useContext } from "react";
import { useParams } from "react-router-dom";
import axios from "axios";
import { AuthContext } from "../../contexts/AuthContext";

export default function useGetInvoiceDetails() {
  const [loading, setLoading] = useState(true);
  const [invoiceDetails, setInvoiceDetails] = useState([]); // Renamed for consistency
  const [error, setError] = useState(null);
  const { idInvoice } = useParams(); // Destructuring idInvoice from URL parameters
  const { token } = useContext(AuthContext);


  useEffect(() => {
    async function getInvoiceDetail() {
      try {
        setLoading(true);
        const response = await axios.get(`${import.meta.env.VITE_API_URL}/api/InvoiceDetails/${idInvoice ? idInvoice : 0}`, {
          headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${token}`,
          },
        });
        setInvoiceDetails(response.data); // Updated setter function name
      } catch (err) {
        setError(err.message || 'An error occurred'); // Updated error handling
      } finally {
        setLoading(false);
      }
    }
    
    getInvoiceDetail();
  }, [token, idInvoice]); // Adding idInvoice to dependency array
  
  return { loading, invoiceDetails, error };
}
