import { useState, useContext, useEffect, useCallback } from 'react';
import axios from 'axios';
import { AuthContext } from "../../contexts/AuthContext";

const useGetPayment = () => {
    const [isPaymentLoading, setIsPaymentLoading] = useState(false);
    const [errorPayment, setErrorPayment] = useState(null);
    const [payments, setPayments] = useState([]);
    const { token } = useContext(AuthContext);

    // Wrapped in useCallback to prevent unnecessary re-creations of the function
    const getPayment = useCallback(async () => {
        setIsPaymentLoading(true);
        setErrorPayment(null);

        try {
            const res = await axios.get(`${import.meta.env.VITE_API_URL}/api/payment-methods`, {
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": `Bearer ${token}`,
                },
            });
            setPayments(res.data);
        } catch (err) {
            setErrorPayment(err);
        } finally {
            setIsPaymentLoading(false);
        }
    }, [token]); // token is a dependency here

     // Fetch checkouts on mount and when token changes
     useEffect(() => {
        getPayment();
    }, [getPayment]);

    return {
        payments,
        isPaymentLoading,
        errorPayment,
        refetchCheckouts: getPayment, // Expose the refetch function
    };
};

export default useGetPayment;
