import { useState, useEffect } from 'react';
import axios from 'axios';

export default function useGetScheduleByCourseId(courseId) {
  const [getScheduleByIdLoading, setGetScheduleByIdLoading] = useState(false);
  const [schedules, setSchedules] = useState([]);
  const [course, setCourse] = useState(null);
  const [error, setError] = useState(null);

  useEffect(() => {
    const getScheduleById = async () => {
      setGetScheduleByIdLoading(true);
      setError(null);
      try {
        const response = await axios.get(`${import.meta.env.VITE_API_URL}/api/Schedule/${courseId}`);
        const data = response.data;
        setCourse(data[0].course);
        setSchedules(data);
      } catch (err) {
        setError(err);
        console.error('Failed to fetch schedule:', err);
      } finally {
        setGetScheduleByIdLoading(false);
      }
    };

    getScheduleById();
  }, [courseId]); // Only re-run the effect if courseId changes


  // Return the state and anything else we want to expose from this hook
  return { schedules, course, getScheduleByIdLoading, error };
}
