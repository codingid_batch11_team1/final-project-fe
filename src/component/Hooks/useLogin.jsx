import { useState } from "react";
import { useNavigate } from "react-router-dom";
import axios from "axios";
import { useAuth } from "../../contexts/AuthContext"; // Corrected the import path

const useLogin = () => {
  const { loginUser } = useAuth();
  const navigate = useNavigate();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [passwordError, setPasswordError] = useState("");
  const [errorMessage, setErrorMessage] = useState("");
  const [emailErrorMessage, setEmailErrorMessage] = useState();
  const [passwordErrorMessage, setPasswordErrorMessage] = useState();

  // Validate the email
  const isEmailValid = (email) => {
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return emailRegex.test(email);
  };

  
  const loginButtonFunc = async () => {
      
  // Validate the email using the isEmailValid function
  if (!email) {
    setEmailErrorMessage("Email is required");
  } else if (!isEmailValid(email)) {
    setEmailErrorMessage("Email must be valid");
  } else {
    setEmailErrorMessage("");
  }
  
  if (password === "") {
    setPasswordErrorMessage("Password is required");
  } else if (password.length < 8) {
    setPasswordErrorMessage("Password should be at least 8 characters");
  } else {
    setPasswordErrorMessage("");
  }
    // Made this function async to use await
    try {
      const res = await axios.post(
        `${import.meta.env.VITE_API_URL}/api/Auth/Login`, // Corrected to use REACT_APP_API_URL
        { Email: email, Password: password }, // Corrected to match the case used in the backend (assuming it is case-sensitive)
        { headers: { "Content-Type": "application/json" } }
      );
      loginUser(res.data); // Pass the token to loginUser function
      if (res.data.role === "ADMIN") navigate("/dashboardadmin");
    } catch (err) {
      if (err.response.status === 404) {
        // Email not found in Database
        setErrorMessage("Email or Password is wrong!");
        alert("Email or Password is wrong!");
      } else {
        // Other errors
        setErrorMessage("An error occurred during login");
        alert(setPasswordError);
      }
    }
  };

  return {
    loginButtonFunc,
    setEmail,
    setPassword,
    errorMessage,
    email,
    password,
    passwordError,
    emailErrorMessage,
    passwordErrorMessage,
  };
};

export default useLogin;
