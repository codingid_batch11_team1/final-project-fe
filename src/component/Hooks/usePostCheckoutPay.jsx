import { useState, useContext } from "react";
import axios from "axios";
import { AuthContext } from "../../contexts/AuthContext";
import { useNavigate } from "react-router-dom";

const usePostCheckoutPay = () => {
  const [isPaying, setIsPaying] = useState(false);
  const [paymentError, setPaymentError] = useState(null);
  const { token } = useContext(AuthContext);
  const navigate = useNavigate();

  const handleError = (err) => {
    if (err.response) {
      let errorMessage;
      switch (err.response.status) {
        case 400:
          errorMessage = err.response.data || "Bad request";
          break;
        case 401:
          errorMessage = err.response.data || "You need to login first";
          break;
        default:
          errorMessage = "An unexpected error occurred";
      }
      return errorMessage;
    }
  };

  const pay = async (paymentData) => {
    setIsPaying(true);
    setPaymentError(null);

    try {
      // Replace '/api/payment/checkout' with your actual endpoint
      const res = await axios.post(
        `${import.meta.env.VITE_API_URL}/api/Checkout/Pay`,
        paymentData,
        {
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${token}`, // Use the token here
          },
        }
      );

      // Handle the response as needed, response.data will contain the data returned from the server
      // console.log('Payment successful', res.data);
      navigate("/purchase-success");
      // Optionally, return some value or call another function here
    } catch (err) {
      const errorMessage = handleError(err);
      setPaymentError(errorMessage);
      alert(errorMessage);
    } finally {
      setIsPaying(false);
    }
  };

  return { isPaying, paymentError, pay };
};

export default usePostCheckoutPay;
