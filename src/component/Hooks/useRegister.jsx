import axios from "axios";
import { useState } from "react";
import { useNavigate } from "react-router-dom";

const useRegister = () => {
  const navigate = useNavigate();
  const [userName, setUserName] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [email, setEmail] = useState('');
  const [errorMessage, setErrorMessage] = useState('');
  const [userNameErrorMessage, setUserNameErrorMessage] = useState('')
  const [emailErrorMessage, setEmailErrorMessage] = useState();
  const [passwordErrorMessage, setPasswordErrorMessage] = useState();
  const [confirmPasswordErrorMessage, setConfirmPasswordErrorMessage] = useState();
  
  const registerButtonFunc = () => {
    if (userName === "") {
      setUserNameErrorMessage("Username is required");
      return;
    }else{ 
      setUserNameErrorMessage("");}

    // Validate the email
    const isEmailValid = (email) => {
      // Gunakan regular expression untuk memeriksa format email
      const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
      return emailRegex.test(email);
    };
    // Validate the email using the isEmailValid function
    if (!email) {
      setEmailErrorMessage("Email is required");
      return;
    } else if (!isEmailValid(email)) {
      setEmailErrorMessage("Email must be valid");
      return;
    } else {
      setEmailErrorMessage("");
    }
  
    if (password === "") {
      setPasswordErrorMessage("Password is required");
      return;
    } else if (password.length < 8) {
      setPasswordErrorMessage("Password should be at least 8 characters");
      return;
    } else{
      setPasswordErrorMessage("");
    }

    if (confirmPassword === "") {
      setConfirmPasswordErrorMessage("Confirm password is required");
      return;
    } else if (confirmPassword.length < 8) {
      setConfirmPasswordErrorMessage("Confirm password should be at least 8 characters");
      return;
    } else{
      setConfirmPasswordErrorMessage("");
    }
    // Check if passwords match
    if (password !== confirmPassword) {
      setErrorMessage("Password and Confirm password must match");
      return;
    } else{
      setErrorMessage("")
    }
    // Make the registration request
    axios
      .post(
        `${import.meta.env.VITE_API_URL}/api/Auth/Register`,
        {
          username: userName,
          password: password,
          email: email,
        },
        {
          headers: {
            "Content-Type": "application/json",
          },
        }
      )
      .then(() => {
        alert("Silahkan cek email!!!");
        setTimeout(() => {
          navigate("/");
        }, 2000);
      })
      .catch((err) => {
        if (err.response) {
          // The request was made and the server responded with an error status
          setErrorMessage(err.response.data);
        } else if (err.request) {
          // The request was made but no response was received
          console.error("Request made but no response received:", err.request);
        } else {
          // Something happened in setting up the request that triggered an Error
          console.error("Error setting up the request:", err.message);
        }
      });
  };

  return {
    registerButtonFunc,
    setUserName,
    setEmail,
    setPassword,
    setConfirmPassword,
    userName,
    email,
    password,
    confirmPassword,
    setErrorMessage,
    setUserNameErrorMessage,
    setEmailErrorMessage,
    setPasswordErrorMessage,
    setConfirmPasswordErrorMessage,
    errorMessage,
    userNameErrorMessage,
    emailErrorMessage,
    passwordErrorMessage,
    confirmPasswordErrorMessage,
  };
};

export default useRegister;
