import { useState, useContext, useCallback } from 'react';
import axios from 'axios';
import { AuthContext } from "../../contexts/AuthContext";

const useRemoveCheckoutById = () => {
    const [isLoading, setIsLoading] = useState(false);
    const [error, setError] = useState(null);
    const { token } = useContext(AuthContext);

    const removeCheckout = useCallback(async (idCheckout) => {
        setIsLoading(true);
        setError(null);

        const config = {
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${token}`,
            },
            data: { // Axios expects the data payload for DELETE requests in the `data` property
                idCheckout: idCheckout
            }
        };

        try {
            await axios.delete(`${import.meta.env.VITE_API_URL}/api/Checkout`, config);
        } catch (err) {
            if (!axios.isCancel(err)) {
                setError(err);
            }
        } finally {
            setIsLoading(false);
        }

    }, [token]); // token is the only dependency

    return {
        isLoading,
        error,
        removeCheckout // return the function so it can be called by components
    };
}

export default useRemoveCheckoutById;
