import { useState } from "react";
import { useNavigate } from "react-router-dom";
import axios from "axios";

const useResetPassword = () => {
  const navigate = useNavigate();
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [errorMessage, setErrorMessage] = useState("");
  const [passwordError, setPasswordError] = useState();
  const [confirmPasswordError, setConfirmPasswordError] = useState();

  const validateForm = () => {
    // Password validation
    if (password === "") {
      setPasswordError("Password is required");
      return false;
    } else if (password.length < 8) {
      setPasswordError("Password should be at least 8 characters");
      return false;
    } else {
      setPasswordError("");
    }

    // Confirm password validation
    if (confirmPassword === "") {
      setConfirmPasswordError("Confirm password is required");
      return false;
    } else if (confirmPassword.length < 8) {
      setConfirmPasswordError("Confirm password should be at least 8 characters");
      return false;
    } else {
      setConfirmPasswordError("");
    }

    // Check if passwords match
    if (password !== confirmPassword) {
      setErrorMessage("Password and Confirm password must match");
      return false;
    } else {
      setErrorMessage("");
    }

    return true;
  };

  const resetButtonFunc = (email, Token) => {
    // Validate the form before initiating the reset
    if (!validateForm()) {
      return;
    }

    axios
      .post(
        `${import.meta.env.VITE_API_URL}/api/Auth/ResetPassword`,
        {
          email: email,
          token: Token,
          newPassword: password,
        },
        {
          headers: {
            "Content-Type": "application/json",
          },
        }
      )
      .then(() => {
        alert("Data has been successfully reset");
        navigate("/login");
      })
      .catch((err) => {
        setErrorMessage(err.response.data);
      });
  };

  return {
    resetButtonFunc,
    setPassword,
    setConfirmPassword,
    password,
    passwordError,
    confirmPassword,
    setErrorMessage,
    errorMessage,
    setPasswordError,
    confirmPasswordError,
  };
};

export default useResetPassword;
