import { styled } from "@mui/material/styles";
import Table from "@mui/material/Table";
import TableCell, { tableCellClasses } from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import { Box, Stack, Typography } from "@mui/material";
import { mainColor } from "../Colors";
import MapInvoiceDetail from "./MapInvoiceDetail";
import useGetInvoiceDetails from "../Hooks/useGetInvoiceDetails";

const StyledTableCell = styled(TableCell)(() => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: mainColor,
    color: 'white',
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));


export default function InvoiceDetail() {
  const { invoiceDetails, loading } = useGetInvoiceDetails();

  if (loading) {
    return
  }

  const invoice = invoiceDetails[0].invoice;
  const totalPrice = invoiceDetails.reduce((sum, data) => {
    return sum + data.course.coursePrice;
  }, 0);

  return (
    <Box
      sx={{
        px: 7,
        display: "flex",
        flexDirection: "column",
        height: "70vh",
      }}
    >
      <Box sx={{ marginTop: "96px", display: "flex" }}>
        <Typography>Home </Typography>
        <Typography sx={{ mx: 1 }}> {">"} </Typography>
        <Typography> Invoice </Typography>
        <Typography sx={{ mx: 1 }}> {">"} </Typography>
        <Typography sx={{ color: mainColor }}> Detail Invoice </Typography>
      </Box>
      <Box
        sx={{ display: "flex", flexDirection: "column", marginTop: "32px" }}
      >
        <Typography
          sx={{ fontWeight: "700", color: "#4F4F4F", marginBottom: "30px" }}
        >
          Details Invoice
        </Typography>

        <Stack direction={"row"} spacing={2} sx={{ marginY: "8" }}>
          <Typography>{"No Invoice : "}</Typography>
          <Typography>{invoice.idInvoice}</Typography>
        </Stack>
        <Stack direction={"row"} spacing={2} justifyContent="space-between">
          <Stack direction={"row"} spacing={8}>
            <Typography>Date : </Typography>
            <Typography>{invoice.formattedDate}</Typography>
          </Stack>
          <Typography>Total Price IDR {totalPrice.toLocaleString()}</Typography>
        </Stack>

        <TableContainer>
          <Table
            sx={{ minWidth: 650, marginTop: "24px" }}
            aria-label="simple table"
          >
            <TableHead sx={{ backgroundColor: mainColor }}>
              <TableRow>
                <StyledTableCell align="center">No</StyledTableCell>
                <StyledTableCell align="center">Course Name </StyledTableCell>
                <StyledTableCell align="center">Type</StyledTableCell>
                <StyledTableCell align="center">Schedule</StyledTableCell>
                <StyledTableCell align="center">Price</StyledTableCell>
              </TableRow>
            </TableHead>

            {/* Add MapInvoiceDetail component here to display course details */}
            <MapInvoiceDetail invoiceDetails={invoiceDetails} />
          </Table>
        </TableContainer>
      </Box>
    </Box>
  );
}
