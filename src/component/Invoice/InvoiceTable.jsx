/* eslint-disable no-unused-vars */
import React from "react";
import { styled } from "@mui/material/styles";
import Table from "@mui/material/Table";
import TableCell, { tableCellClasses } from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import { mainColor } from "../Colors";
import { Box, Typography } from "@mui/material";
import MapInvoice from "./MapInvoice"; // Import MapInvoice component

const StyledTableCell = styled(TableCell)(() => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: mainColor(),
    color: 'white',
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));

export default function InvoiceTable() {
  return (
    <Box sx={{ px: 7, minHeight: '60vh' }}>
      <Box sx={{ marginTop: "96px", display: "flex" }}>
        <Typography>Home </Typography>
        <Typography sx={{ mx: 1 }}> {">"} </Typography>
        <Typography sx={{ color: mainColor() }}> Invoice </Typography>
      </Box>
      <Box
        sx={{ display: "flex", flexDirection: "column", marginTop: "32px", marginBottom: "100px" }}
      >
        <Typography>Menu Invoice</Typography>
        <TableContainer>
          <Table
            sx={{ minWidth: 650, marginTop: "24px" }}
            aria-label="simple table"
          >
            <TableHead sx={{ backgroundColor: mainColor() }}>
              <TableRow>
                <StyledTableCell align="center">No</StyledTableCell>
                <StyledTableCell align="center">No. Invoice</StyledTableCell>
                <StyledTableCell align="center">Date</StyledTableCell>
                <StyledTableCell align="center">Total Course</StyledTableCell>
                <StyledTableCell align="center">Total Price</StyledTableCell>
                <StyledTableCell align="center">Action</StyledTableCell>
              </TableRow>
            </TableHead>
            {/* Include MapInvoice component to display data */}
            <MapInvoice />
          </Table>
        </TableContainer>
      </Box>
    </Box>
  );
}
