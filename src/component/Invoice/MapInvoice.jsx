/* eslint-disable no-unused-vars */
import React, { useMemo } from 'react';
import { Button, TableBody, TableCell, TableRow } from '@mui/material';
import useGetInvoiceDetails from '../Hooks/useGetInvoiceDetails';
import { useNavigate } from 'react-router-dom';
import OutlinedButton from '../Button/OutlinedButton'

const MapInvoice = () => {
  const { loading, invoiceDetails } = useGetInvoiceDetails();
  const navigate = useNavigate();

  const handleDetailsClick = (invoiceId) => {
    navigate(`/invoice_detail/${invoiceId}`);
  };

  const groupedInvoices = useMemo(() => {
    const groups = new Map();
    invoiceDetails.forEach((detail) => {
      if (!groups.has(detail.invoice.idInvoice)) {
        groups.set(detail.invoice.idInvoice, {
          ...detail.invoice,
          details: [],
          totalCourse: 0,
          totalPrice: 0,
        });
      }
      const group = groups.get(detail.invoice.idInvoice);
      group.details.push(detail);
      group.totalCourse += 1; // Assuming each detail represents one course
      group.totalPrice += detail.course.coursePrice;
    });
    return Array.from(groups.values());
  }, [invoiceDetails]);

  const formatCurrency = (value) => {
    const formattedValue = new Intl.NumberFormat('id-ID', {
      minimumFractionDigits: 0,
      maximumFractionDigits: 0,
    }).format(value);

    return `IDR ${formattedValue}`;
  };

  if (loading) {
    return
  }

  return (
    <TableBody>
      {groupedInvoices.map((invoiceItem, index) => (
        <TableRow
          key={invoiceItem.idInvoice}
          style={{ backgroundColor: index % 2 === 0 ? 'transparent' : 'rgba(121, 11, 10, 0.10)' }}
        >
          <TableCell align="center">{index + 1}</TableCell>
          <TableCell align="center">{invoiceItem.idInvoice}</TableCell>
          <TableCell align="center">{invoiceItem.paymentDate}</TableCell>
          <TableCell align="center">{invoiceItem.totalCourse}</TableCell>
          <TableCell align="center">{formatCurrency(invoiceItem.totalPrice)}</TableCell>
          <TableCell align="center">
            <OutlinedButton
            onClick={() => handleDetailsClick(invoiceItem.idInvoice)}
            title={"Details"}
            fontWeight="bold"
            />
          </TableCell>
        </TableRow>
      ))}
    </TableBody>
  );
};

export default MapInvoice;