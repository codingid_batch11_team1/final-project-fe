/* eslint-disable no-unused-vars */
import React from "react";
import { TableBody, TableCell, TableRow } from "@mui/material";
import PropTypes from "prop-types";

const MapInvoiceDetail = ({ invoiceDetails }) => {
  const formatCurrency = (value) => {
    const formattedValue = new Intl.NumberFormat("id-ID", {
      minimumFractionDigits: 0,
      maximumFractionDigits: 0,
    }).format(value);

    return `IDR ${formattedValue}`;
  };

  return (
    <TableBody>
      {invoiceDetails.map((invoiceItem, index) => (
        <TableRow
          key={invoiceItem.idInvoiceDetails}
          style={{
            backgroundColor:
              index % 2 === 0 ? "transparent" : "rgba(91, 73, 71, 0.2)",
          }}
        >
          <TableCell align="center">{index + 1}</TableCell>
          <TableCell align="center">{invoiceItem.course.courseName}</TableCell>
          <TableCell align="center">
            {invoiceItem.category.categoryName}
          </TableCell>
          <TableCell align="center">
            {invoiceItem.schedule.formattedDate}
          </TableCell>
          <TableCell align="center">
            {formatCurrency(invoiceItem.course.coursePrice)}
          </TableCell>
        </TableRow>
      ))}
    </TableBody>
  );
};

MapInvoiceDetail.propTypes = {
  invoiceDetails: PropTypes.arrayOf(
    PropTypes.shape({
      idInvoiceDetails: PropTypes.number.isRequired,
      course: PropTypes.shape({
        courseName: PropTypes.string.isRequired,
        coursePrice: PropTypes.number.isRequired,
        // Add other properties of the 'course' object if any
      }).isRequired,
      category: PropTypes.shape({
        categoryName: PropTypes.string.isRequired,
        // Add other properties of the 'category' object if any
      }).isRequired,
      schedule: PropTypes.shape({
        formattedDate: PropTypes.string.isRequired,
        // Add other properties of the 'schedule' object if any
      }).isRequired,
      // Add other properties of the 'invoiceItem' object if any
    })
  ).isRequired,
};

export default MapInvoiceDetail;
