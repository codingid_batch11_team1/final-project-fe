import { Box, Container, Grid, Typography } from "@mui/material";

export default function LandingBenefit() {
  return (
    <Box>
      <Container maxWidth="lg">
        <Typography variant="h3" gutterBottom>
          Gets your benefit
        </Typography>
        <Grid container spacing={10}>
          <Grid item xs={12} md={8}>
            <Typography align="justify">
              Sed ut perspiciatis unde omnis iste natus error sit voluptatem
              accusantium doloremque laudantium, totam rem aperiam, eaque ipsa
              quae ab illo inventore veritatis et quasi architecto beatae vitae
              dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit
              aspernatur aut odit aut fugit, sed quia consequuntur magni dolores
              eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam
              est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci
              velit, sed quia non numquam. Neque porro quisquam est, qui dolorem
              ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia
              non numquam eius modi tempora incidunt ut labore et dolore magnam
              aliquam quaerat voluptatem.
            </Typography>
          </Grid>
          <Grid item xs={12} md={4}>
            <Box sx={{ display: "flex", justifyContent: "center" }}>
              <img
                src="/landingKategori.png"
                alt="Email Confirm Success"
                style={{
                  maxWidth: "373px",
                  height: "auto",
                  marginTop: "-20px",
                }}
              />
            </Box>
          </Grid>
        </Grid>
      </Container>
    </Box>
  );
}
