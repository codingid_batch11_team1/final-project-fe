import { Box, Typography, Grid, useTheme, useMediaQuery } from '@mui/material';

export default function LandingCard() {
  const theme = useTheme();
  const isDesktop = useMediaQuery(theme.breakpoints.up('md'));

  return (
    <Box
      mt={8}
      sx={{
        backgroundImage: 'url(/backgroundImage.png)',
        backgroundSize: 'cover',
        backgroundPosition: 'center',
        color: '#fff',
        height: '100vh',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
      }}
    >
      <Typography variant={isDesktop ? 'h2' : 'h4'} textAlign="center" mb={10}>
        We provide driving lessons for various types of cars
      </Typography>
      <Typography variant={isDesktop ? 'h3' : 'h5'} textAlign="center" mb={10}>
        Professional staff who are ready to help you to become a much-needed reliable driver
      </Typography>
      <Grid container spacing={2} justifyContent='center' mt={4}>
        <Grid item xs={12} md={3} sx={{ textAlign: 'center' }}>
          <Typography variant={isDesktop ? 'h1' : 'h3'}>50+</Typography>
          <Typography variant={isDesktop ? 'h6' : 'body1'}>A class ready to make you a reliable driver</Typography>
        </Grid>
        <Grid item xs={12} md={3} sx={{ textAlign: 'center' }}>
          <Typography variant={isDesktop ? 'h1' : 'h3'}>20+</Typography>
          <Typography variant={isDesktop ? 'h6' : 'body1'}>Professional workforce with great experience</Typography>
        </Grid>
        <Grid item xs={12} md={3} sx={{ textAlign: 'center' }}>
          <Typography variant={isDesktop ? 'h1' : 'h3'}>10+</Typography>
          <Typography variant={isDesktop ? 'h6' : 'body1'}>Cooperate with driver service partners</Typography>
        </Grid>
      </Grid>
    </Box>
  );
}
