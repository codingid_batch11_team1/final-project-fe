/* eslint-disable no-unused-vars */
import * as React from "react";
import PropTypes from "prop-types";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import CssBaseline from "@mui/material/CssBaseline";
import Divider from "@mui/material/Divider";
import Drawer from "@mui/material/Drawer";
import IconButton from "@mui/material/IconButton";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemText from "@mui/material/ListItemText";
import MenuIcon from "@mui/icons-material/Menu";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import OtomobilLogo from "../assets/otomobilLogo.svg";
import Logo from "/logo.png";
import { mainColor } from "./Colors";
import { Link, NavLink } from "react-router-dom";
import Avatar from "@mui/material/Avatar";
import { Stack } from "@mui/system";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";
import LogoutIcon from "@mui/icons-material/Logout";
import { useContext } from "react";
import { AuthContext } from "../contexts/AuthContext";

const drawerWidth = 240;

export default function LandingHeader(props) {
  const { window } = props;
  const [mobileOpen, setMobileOpen] = React.useState(false);
  const { isAuth, logout } = useContext(AuthContext);

  const handleDrawerToggle = () => {
    setMobileOpen((prevState) => !prevState);
  };

  const drawer = (
    <Box onClick={handleDrawerToggle} sx={{ textAlign: "center" }}>
      <NavLink to="/" style={{ textDecoration: "none", color: "black" }}>
        <Box>
          <img
            src={OtomobilLogo}
            alt="Otomobil Logo"
            style={{ marginBottom: "-30px", height: "50%" }}
          />
          <Typography variant="h6" sx={{ my: 1 }}>
            Otomobil
          </Typography>
        </Box>
      </NavLink>
      <Divider />
      <List>
        <NavLink
          to="/checkout"
          style={{ textDecoration: "none", color: "black" }}
        >
          <ListItem disablePadding>
            <ListItemButton sx={{ textAlign: "center" }}>
              <ListItemText primary="Checkout" />
            </ListItemButton>
          </ListItem>
        </NavLink>
        <NavLink to="/myclass" style={{ textDecoration: "none", color: "black" }}>
          <ListItem disablePadding>
            <ListItemButton sx={{ textAlign: "center" }}>
              <ListItemText primary="My Class" />
            </ListItemButton>
          </ListItem>
        </NavLink>
        <NavLink
          to="/invoice"
          style={{ textDecoration: "none", color: "black" }}
        >
          <ListItem disablePadding>
            <ListItemButton sx={{ textAlign: "center" }}>
              <ListItemText primary="Invoice" />
            </ListItemButton>
          </ListItem>
        </NavLink>
        <NavLink to="" style={{ textDecoration: "none", color: "black" }}>
          <ListItem disablePadding>
            <ListItemButton sx={{ textAlign: "center" }}>
              <ListItemText primary="Profile" />
            </ListItemButton>
          </ListItem>
        </NavLink>
        <Divider />
        <NavLink onClick={() => logout()} style={{ textDecoration: "none", color: "black" }}>
          <ListItem disablePadding>
            <ListItemButton sx={{ textAlign: "center" }}>
              <ListItemText primary="Log Out" />
            </ListItemButton>
          </ListItem>
        </NavLink>
      </List>
    </Box>
  );

  const container =
    window !== undefined ? () => window().document.body : undefined;

  return (
    <Box sx={{ display: "flex" }}>
      <CssBaseline />
      <AppBar
        component="nav"
        sx={{ backgroundColor: "white", color: "black", boxShadow: "none" }}
      >
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            edge="start"
            onClick={handleDrawerToggle}
            sx={{ mr: 2, display: { sm: "none" } }}
          >
            <MenuIcon />
          </IconButton>
          <Box
            component="div"
            sx={{
              flexGrow: 1,
              display: { xs: "none", sm: "flex" },
              alignItems: "center",
              gap: 2,
            }}
          >
            <Link to={"/"}>
              <img
                src={Logo}
                alt="Otomobil Logo"
                height={50}
                style={{ verticalAlign: "middle" }}
              />
            </Link>
          </Box>
          <Box sx={{ display: { xs: "none", sm: "block" } }}>
            <Stack direction="row" spacing={2} alignItems="center">
              <NavLink to="/checkout">
                <ShoppingCartIcon
                  sx={{
                    marginTop: "4px",
                    marginRight: "20px",
                    borderRadius: "3px",
                    color: mainColor(),
                    bgcolor: "white",
                    "&:hover": {
                      padding: "5px",
                      color: "white",
                      backgroundColor: "#6a0008",
                    },
                  }}
                />
              </NavLink>
              <NavLink to="/myclass" style={{ textDecoration: "none" }}>
                <Typography
                  sx={{
                    padding: "3px",
                    color: mainColor(),
                    borderRadius: "3px",
                    marginRight: "20px",
                    fontSize: "16px",
                    "&:hover": {
                      color: "white",
                      backgroundColor: "#6a0008",
                    },
                  }}
                >
                  My Class
                </Typography>
              </NavLink>
              <NavLink to="/invoice" style={{ textDecoration: "none" }}>
                <Typography
                  sx={{
                    padding: "3px",
                    color: mainColor(),
                    borderRadius: "3px",
                    marginRight: "20px",
                    fontSize: "16px",
                    "&:hover": {
                      color: "white",
                      backgroundColor: "#6a0008",
                    },
                  }}
                >
                  Invoice
                </Typography>
              </NavLink>
              <Typography>|</Typography>
              <Button>
                <Avatar
                  
                  sx={{
                    color: mainColor(),
                    bgcolor: "white",
                    "&:hover": {
                      color: "white",
                      backgroundColor: "#6a0008",
                    },
                  }}
                />
              </Button>
              <LogoutIcon
                onClick={() => logout()}
                sx={{
                  marginTop: "4px",
                  marginRight: "20px",
                  borderRadius: "3px",
                  color: mainColor(),
                  bgcolor: "white",
                  "&:hover": {
                    padding: "5px",
                    color: "white",
                    backgroundColor: "#6a0008",
                  },
                }}
              />
            </Stack>
          </Box>
        </Toolbar>
      </AppBar>
      <nav>
        <Drawer
          container={container}
          variant="temporary"
          open={mobileOpen}
          onClose={handleDrawerToggle}
          ModalProps={{
            keepMounted: true,
          }}
          sx={{
            display: { xs: "block", sm: "none" },
            "& .MuiDrawer-paper": {
              boxSizing: "border-box",
              width: drawerWidth,
            },
          }}
        >
          {drawer}
        </Drawer>
      </nav>
    </Box>
  );
}

LandingHeader.propTypes = {
  /**
   * Injected by the documentation to work in an iframe.
   * You won't need it on your project.
   */
  window: PropTypes.func,
};
