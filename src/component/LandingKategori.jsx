/* eslint-disable no-unused-vars */
import { Box, Container, Grid, Typography } from "@mui/material";
import { Link } from "react-router-dom";
import useGetCategory from "./Hooks/useGetCategory";

export default function LandingKategori() {
  const { data: data1 } = useGetCategory();

  return (
    <Box display="flex" flexDirection="column" alignItems="center" mb={10}>
      <Container maxWidth="md">
        <Typography variant="h4" textAlign="center" mt={7} mb={7}>
          More car you can choose
        </Typography>
        <Grid container spacing={5} justifyContent="center" alignItems="center">
          {data1.map((data) => (
            <Grid item key={data.idCategory} xs={6} sm={6} md={3}>
              <Link
                to={`landing_list_menu_class_page/${data.idCategory}`}
                style={{ textDecoration: "none", color: "black" }}
              >
                <Box>
                  <img
                    src={`${import.meta.env.VITE_API_URL}/${data.categoryImage}`}
                    style={{ width: "50%", display: "block", margin: "auto" }}
                    alt={data.categoryName}
                  />
                  <Typography textAlign="center">
                    {data.categoryName}
                  </Typography>
                </Box>
              </Link>
            </Grid>
          ))}
        </Grid>
      </Container>
    </Box>
  );
}
