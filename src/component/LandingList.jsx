import { Box, Container, Grid } from "@mui/material";
import CourseCard from "./card/CourseCard";
import useGetCourseCard from "./Hooks/useGetCourseCard";

export default function LandingList() {
  const { courses, loading } = useGetCourseCard();

  if (loading) {
    return <h1>Loading...</h1>;
  }

  return (
    <Box display="flex" flexDirection="column" mb={10}>
      <Container maxWidth="lg">
        <Grid container spacing={3} alignItems="center" justifyContent="center">
          {courses.map((course) => (
            <Grid item key={course.courseId} xs={8} sm={6} md={4}>
              <CourseCard maxText={20} courses={course} />
            </Grid>
          ))}
        </Grid>
      </Container>
    </Box>
  );
}
