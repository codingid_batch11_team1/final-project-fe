/* eslint-disable no-unused-vars */
import Card from "@mui/material/Card";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";
import { CardActionArea, Container } from "@mui/material";
import { useParams } from "react-router";
import useGetCategoryById from "./Hooks/useGetCategorybyId";

export default function LandingListMenuClass() {
  const { idCategory } = useParams();
  const { data, error, isLoading } = useGetCategoryById(idCategory);

  if (isLoading) {
    return <div>Loading...</div>;
  }

  return (
    <Card sx={{ maxWidth: "100%", maxHeight: "100%", marginTop: "4rem" }}>
      <CardActionArea key={data.idCategory}>
        <CardMedia
          component="img"
          src={`${import.meta.env.VITE_API_URL}/${data.categoryBanner}`}
          alt="Landing List Menu Class"
          // height={600}
          sx={{ aspectRatio: "16/4" }}
        />
        <Container maxWidth="lg" sx={{ mt: 7 }}>
          <Typography gutterBottom variant="h5" component="div">
            {data.categoryName}
          </Typography>
          <Typography variant="body2" color="text.secondary" mb={7}>
            {data.categoryDetail}
          </Typography>
        </Container>
      </CardActionArea>
    </Card>
  );
}
