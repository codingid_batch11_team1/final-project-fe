/* eslint-disable no-unused-vars */
import { useState, useContext } from "react";
import { Container, Typography, Grid, Box, FormControl } from "@mui/material";
import TextFieldForm from "./Textfield/TextFieldForm";
import { Link } from "react-router-dom";
import ContainedButton from "./Button/ContainedButton";
import { AuthContext } from "../contexts/AuthContext";
import useLogin from "./Hooks/useLogin";

export default function LoginForm() {
  const { loginUser } = useContext(AuthContext);
  const [loading, setLoading] = useState(false);

  const {
    loginButtonFunc,
    setEmail,
    setPassword,
    email,
    password,
    passwordErrorMessage,
    emailErrorMessage
  } = useLogin();

  const handleSubmit = (e) => {
    e.preventDefault();
    loginButtonFunc();
  };

  return (
    <Box
      display="flex"
      alignItems="center"
      justifyContent="center"
      style={{ height: "100vh" }}
    >
      <Container maxWidth="md">
        <Typography variant="h4" align="left" color={"#790B0A"}>
          Welcome Back!
        </Typography>
        <Typography variant="h6" align="left" mb={6} mt={2}>
          Please login first
        </Typography>
        <form onSubmit={handleSubmit}>
          <Grid container spacing={3}>
            <Grid item xs={12} sm={12}>
              <TextFieldForm
                label="Email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                disabled={loading}
              />
              <Typography variant="h6" align="left" color="error">
                {emailErrorMessage}
              </Typography>              
            </Grid>
            <Grid item xs={12} sm={12}>
              <TextFieldForm
                label="Password"
                type="password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                disabled={loading}
              />
              <Typography variant="h6" align="left" color="error">
                {passwordErrorMessage}
              </Typography>
            </Grid>
            <Grid item xs={12}>
              <Typography>
                Forgot Password?{" "}
                <Link
                  to={"/resetPassword"}
                  style={{ textDecoration: "none" }}
                  color="#2F80ED"
                >
                  Click Here
                </Link>
              </Typography>
            </Grid>
            <Grid item xs={12}>
              <Box textAlign="right" mt={2}>
                <ContainedButton
                  title="Login"
                  type="submit"
                  disabled={loading}
                  // onClick={handleSubmit}
                >
                  {loading ? "Logging in..." : "Login"}
                </ContainedButton>
              </Box>
            </Grid>
          </Grid>
        </form>
        <Typography align="center" mt={12}>
          Dont have account?{" "}
          <Link
            to={"/register"}
            style={{ textDecoration: "none" }}
            color="#2F80ED"
          >
            Sign Up here
          </Link>
        </Typography>
      </Container>
    </Box>
  );
}
