import React from "react";
import { Box, Container, Divider, Stack, Typography } from "@mui/material";
import { mainColor } from "./Colors";
import useGetInvoiceDetails from "./Hooks/useGetInvoiceDetails";

export default function MyClass() {
  const { invoiceDetails, loading } = useGetInvoiceDetails();

  if (loading) {
    return <div>Loading...</div>; // Display a loading indicator
  }

  // console.log(invoiceDetails);

  return (
    <Box sx={{ display: "flex", flexDirection: "column", minHeight: "100vh" }}>
      {" "}
      {/* Flex layout wrapper */}
      <Container maxWidth="xxl" sx={{ flexGrow: 1, paddingBottom: "100px" }}>
        {" "}
        {/* flexGrow to take available space and paddingBottom to avoid overlap with footer */}
        <Box
          sx={{
            paddingX: { xs: "0", md: "72px", lg: "72px" },
            width: "100%",
            marginTop: "100px",
          }}
        >
          {invoiceDetails.map((data) => (
            <React.Fragment key={data.idInvoiceDetails}>
              <Box
                sx={{
                  display: "flex",
                  alignItems: "center",
                  marginY: "24px",
                  justifyContent: {
                    xs: "center",
                    md: "space-between",
                  },
                }}
              >
                <Box
                  sx={{
                    width: { xs: "90%", lg: "90%" },
                    display: "flex",
                    alignItems: "center",
                  }}
                >
                  <Box
                    component="img"
                    sx={{
                      width: { xs: 100, md: 150, lg: 200 },
                    }}
                    alt={data.course.courseName}
                    src={`${import.meta.env.VITE_API_URL}${
                      data.course.courseImage
                    }`}
                  />
                  <Stack
                    direction="column"
                    mx={4}
                    sx={{ marginY: { xs: "20px", md: "10px", lg: "5px" } }}
                  >
                    <Typography>{data.category.categoryName}</Typography>
                    <Typography
                      fontWeight="600"
                      fontSize={{ xs: "16px", md: "20px", lg: "24px" }}
                    >
                      {data.course.courseName}
                    </Typography>
                    <Typography
                      color={mainColor}
                      fontWeight="500"
                      fontSize={{ xs: "12px", md: "16px", lg: "20px" }}
                    >
                      Schedule : {data.schedule.formattedDate}
                    </Typography>
                  </Stack>
                </Box>
              </Box>
              <Divider />
            </React.Fragment>
          ))}
        </Box>
      </Container>
    </Box>
  );
}
