import { Stack, Container, Typography, Box, Grid } from "@mui/material";
import { Link } from "react-router-dom";
import ContainedButton from "./Button/ContainedButton";
import OutlinedButton from "./Button/OutlinedButton";
import TextFieldForm from "./Textfield/TextFieldForm";
import useResetPassword from "./Hooks/useResetPassword";
import { useState } from "react";

export default function NewPassForm() {
  const queryParameters = new URLSearchParams(window.location.search);
  const email = queryParameters.get("email");
  const Token = queryParameters.get("token");

  // console.log(email)
  // console.log(Token)
  const {
    resetButtonFunc,
    setPassword,
    setConfirmPassword,
    password,
    passwordError,
    confirmPassword,
    errorMessage,
    confirmPasswordError,
  } = useResetPassword();
  const [passwordError1, setPasswordError1] = useState("");
  const [confirmPasswordError1, setConfirmPasswordError1] = useState("");
  const handleNewPass = (e) => {
    e.preventDefault();

    if (password === "" && confirmPassword === "") {
      // If either username or password is empty, set the error states
      setPasswordError1(true);
      setConfirmPasswordError1(true);
    } else if (password === "") {
      setPasswordError1(true);
    } else if (confirmPassword === "") {
      setConfirmPasswordError1(true);
    }

    resetButtonFunc(email, Token);
  };

  return (
    <Box
      display="flex"
      alignItems="center"
      justifyContent="center"
      style={{ height: "100vh" }}
    >
      <Container maxWidth="md">
        <Typography variant="h4" align="left" mb={6}>
          Create Password
        </Typography>
        <Typography variant="h6" align="left" mb={6} mt={2}>
          {errorMessage}
        </Typography>
        <form onSubmit={handleNewPass}>
          <Grid container spacing={3}>
            <Grid item xs={12} sm={12}>
              <TextFieldForm
                error={!!passwordError1}
                helperText={passwordError1}
                label="New Password"
                type="password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />
              <Typography variant="h6" align="left" color="error">
                {passwordError}
              </Typography>
            </Grid>
            <Grid item xs={12} sm={12}>
              <TextFieldForm
                error={!!confirmPasswordError1}
                helperText={confirmPasswordError1}
                label="Confirm New Password"
                type="password"
                value={confirmPassword}
                onChange={(e) => setConfirmPassword(e.target.value)}
              />
              <Typography variant="h6" align="left" color="error">
                {confirmPasswordError}
              </Typography>
            </Grid>
            <Grid item xs={12}>
              <Box mt={2}>
                <Stack flexDirection="row" justifyContent="flex-end">
                  <Link to={"/login"}>
                    <OutlinedButton title="Cancel"></OutlinedButton>
                  </Link>
                  <ContainedButton
                    title="Submit"
                    type="submit"
                  ></ContainedButton>
                </Stack>
              </Box>
            </Grid>
          </Grid>
        </form>
      </Container>
    </Box>
  );
}
