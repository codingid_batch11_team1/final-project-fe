import { Typography, Grid, Box } from '@mui/material';
import GambarEmailSuccess from '../assets/GambarEmailConfirmSuccess.png'
import { Link } from 'react-router-dom';
import ContainedButtonDetailClass from './Button/ContainedButtonDetailClass';
import OutlinedButtonDetailClass from './Button/OutlinedButtonDetailClass';

function PurchaseSuccess() {

    return (
        <Box
            display="flex"
            alignItems="center"
            justifyContent="center"
            style={{ height: '100vh' }}
        >
            <Grid container direction="column" spacing={5} alignItems="center">
                <Grid item>
                    <img src={GambarEmailSuccess} alt="Email Confirm Success" style={{ maxWidth: '200px', height: 'auto' }} />
                </Grid>

                <Grid item>
                    <Typography variant='h4' align='center' color={'#790B0A'}>
                        Purchase Successfully
                    </Typography>
                </Grid>
                <Grid item>
                    <Typography align='center' variant='h6' mt={-4}>
                        That’s Great! We’re ready for driving day
                    </Typography>
                </Grid>
                <Grid item container justifyContent={"center"}>
                    <Grid item>
                        <Link to={'/'}>
                            <OutlinedButtonDetailClass
                                title='Back to Home'
                                height='50px'
                            />
                        </Link>
                    </Grid>
                    <Grid item ml={3}>
                        <Link to={'/invoice'}>
                            <ContainedButtonDetailClass
                                title='Open Invoice'
                                height='50px'
                            />
                        </Link>
                    </Grid>
                </Grid>
            </Grid>
        </Box>
    );
}

export default PurchaseSuccess;
