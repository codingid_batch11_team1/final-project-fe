/* eslint-disable no-unused-vars */
import Header from "../component/Header";
import LandingHeader from "../component/LandingHeader";
import React, { useContext } from "react";
import { AuthContext } from "../contexts/AuthContext";

export default function RealHeader() {
  const { isAuth, logout } = useContext(AuthContext);

  return <header>{isAuth ? <LandingHeader /> : <Header />}</header>;
}
