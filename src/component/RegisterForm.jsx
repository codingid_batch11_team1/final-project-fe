import { useState } from "react";
import {
  Container,
  Typography,
  Stack,
  Box,
  FormControl,
  Grid,
} from "@mui/material";
import { Link } from "react-router-dom";
import ContainedButton from "./Button/ContainedButton";
import TextFieldForm from "./Textfield/TextFieldForm";
import useRegister from "./Hooks/useRegister";

export default function RegisterForm() {
  const {
    registerButtonFunc,
    setUserName,
    setEmail,
    setPassword,
    setConfirmPassword,
    userName,
    email,
    password,
    confirmPassword,
    errorMessage,
    userNameErrorMessage,
    emailErrorMessage,
    passwordErrorMessage,
    confirmPasswordErrorMessage,
  } = useRegister();

  const [userNameError, setUserNameError] = useState("");
  const [emailError, setEmailError] = useState("");
  const [passwordError, setPasswordError] = useState("");
  const [confirmPasswordError, setConfirmPasswordError] = useState("");

  const handleRegister = (e) => {
    e.preventDefault();

    // console.log("Start handleRegister");

    // Perform register validation here
    if (
      userName === "" &&
      password === "" &&
      email === "" &&
      confirmPassword === ""
    ) {
      // If either username or password is empty, set the error states
      setPasswordError(true);
      setUserNameError(true);
      setConfirmPasswordError(true);
      setEmailError(true);
    } else if (password === "") {
      setPasswordError(true);
    } else if (userName === "") {
      setUserNameError(true);
    } else if (email === "") {
      setEmailError(true);
    } else if (confirmPassword === "") {
      setConfirmPasswordError(true);
    }
    // Call your login function here
    registerButtonFunc();

    // console.log("End handleRegister");
  };

  return (
    <Box
      display="flex"
      alignItems="center"
      justifyContent="center"
      style={{ height: "100vh" }}
    >
      <Container maxWidth="md">
        <Typography variant="h4" align="left" color={"#790B0A"}>
          {"Let's Join our course!"}
        </Typography>
        <Typography variant="h6" align="left" mb={6} mt={2}>
          Please register first
        </Typography>
        <Typography variant="h6" align="left" mb={6} mt={2}>
          {errorMessage}
        </Typography>
        <FormControl>
          <Grid container spacing={3}>
            <Grid item xs={12} sm={12}>
              <TextFieldForm
                error={!!userNameError}
                helperText={userNameError}
                label="Name"
                value={userName}
                onChange={(e) => setUserName(e.target.value)}
              />
              <Typography variant="h6" align="left" color="error">
                {userNameErrorMessage}
              </Typography>
            </Grid>

            <Grid item xs={12} sm={12}>
              <TextFieldForm
                error={!!emailError}
                helperText={emailError}
                label="Email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
              <Typography variant="h6" align="left" color="error">
                {emailErrorMessage}
              </Typography>
            </Grid>

            <Grid item xs={12} sm={12}>
              <TextFieldForm
                error={!!passwordError}
                helperText={passwordError}
                label="Password"
                type="password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />
              <Typography variant="h6" align="left" color="error">
                {passwordErrorMessage}
              </Typography>
            </Grid>

            <Grid item xs={12} sm={12}>
              <TextFieldForm
                error={!!confirmPasswordError}
                helperText={confirmPasswordError}
                label="Confirm Password"
                type="password"
                value={confirmPassword}
                onChange={(e) => setConfirmPassword(e.target.value)}
              />
              <Typography variant="h6" align="left" color="error">
                {confirmPasswordErrorMessage}
              </Typography>
            </Grid>
          </Grid>
          <Stack
            flexDirection="row"
            justifyContent="flex-end"
            useFlexGap
            mt={4}
            spacing={{ xs: 1, lg: 2 }}
          >
            <Link to={""}>
              <ContainedButton
                title="Sign Up"
                onClick={handleRegister}
              ></ContainedButton>
            </Link>
          </Stack>
        </FormControl>
        <Typography align="center" mt={12}>
          Have an account?{" "}
          <Link to={"/login"} style={{ textDecoration: "none" }}>
            Login here
          </Link>
        </Typography>
      </Container>
    </Box>
  );
}
