import { Stack, Container, Typography, Box, Grid } from "@mui/material";
import { Link } from "react-router-dom";
import ContainedButton from "./Button/ContainedButton";
import OutlinedButton from "./Button/OutlinedButton";
import TextFieldForm from "./Textfield/TextFieldForm";
import useForgot from "./Hooks/useForgot";

export default function ResetPassForm() {
  const { email, setEmail, forgotButtonFunc, emailErrorMessage, } = useForgot();

  const handleForgotSubmit = (e) => {
    e.preventDefault();
    forgotButtonFunc();
  };
  return (
    <Box
      display="flex"
      alignItems="center"
      justifyContent="center"
      style={{ height: "100vh" }}
    >
      <Container maxWidth="md">
        <Typography variant="h4" align="left">
          Reset Password
        </Typography>
        <Typography variant="h6" sx={{ color: "#4F4F4F" }} mb={8} mt={2}>
          Send link new password to your email address
        </Typography>
        <form onSubmit={handleForgotSubmit}>
          <Grid container spacing={3}>
            <Grid item xs={12} sm={12}>
              <TextFieldForm
                width="850px"
                label="Email"
                required
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
              <Typography variant="h6" align="left" color="error">
                {emailErrorMessage}
              </Typography>              
            </Grid>
            <Grid item xs={12} sm={12}>
              <Box mt={2}>
                <Stack flexDirection="row" justifyContent="flex-end">
                  <Link to={"/login"}>
                    <OutlinedButton title="Cancel"></OutlinedButton>
                  </Link>
                  <ContainedButton
                    title="Confirm"
                    type="submit"
                  ></ContainedButton>
                </Stack>
              </Box>
            </Grid>
          </Grid>
        </form>
      </Container>
    </Box>
  );
}
