/* eslint-disable react/prop-types */
import { TextField } from "@mui/material";
import { PropTypes } from "prop-types";

function TextFieldForm({ label, value, type, onChange, width, error }) {
  return (
    <TextField
      error={error}
      fullWidth
      required
      size="small"
      variant="outlined"
      label={label}
      type={type}
      value={value}
      name={value}
      onChange={onChange}
      sx={{ width: width }}
    />
  );
}

TextFieldForm.propTypes = {
  label: PropTypes.string,
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  type: PropTypes.string,
};

export default TextFieldForm;
