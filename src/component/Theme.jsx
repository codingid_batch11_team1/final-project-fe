import { createTheme } from '@mui/material/styles';

const Theme = createTheme({
    typography: {
        allVariants : {
            fontFamily: 'Montserrat',
            textTransform : 'none',
            textDecoration: 'none',
        },
    },
  });

export default Theme;