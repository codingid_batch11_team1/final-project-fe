/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
import { Card, CardContent, CardMedia, Typography } from "@mui/material";
import { Link } from "react-router-dom";

export default function CourseCard({ courses, maxText }) {
    // const courseImage = courses.courseImage ? `${import.meta.env.VITE_API_URL}/${courses.courseImage}` : '';
    
    // console.log(courses.category.categoryName);
    const formatCurrency = (value) => {
      const formattedValue = new Intl.NumberFormat('id-ID', {
        minimumFractionDigits: 0,
        maximumFractionDigits: 0,
      }).format(value);
    
      return `IDR ${formattedValue}`;
    };
    
    
  const cutText = (text) => {
    if (text.length < maxText) {
      return text;
    }

    const cuttedText = text.slice(0, maxText);
    return cuttedText + "...";
  };

  return (
    <Link
      to={`/class_detail_buy/${courses.category.idCategory}/${courses.courseId}`}
      style={{ textDecoration: "none" }}
    >
      <Card sx={{ maxWidth: 345 }} elevation={0}>
        <CardMedia
          //   sx={{ height: 140 }}
          sx={{ aspectRatio: "16/9" }}
          component="img" // Use the "img" component
          src={`${import.meta.env.VITE_API_URL}/${courses.courseImage}`} // Set the source to the image URL
          alt={courses.category.categoryName}
        />
        <CardContent>
          <Typography variant="body2" color="text.secondary">
            {courses.category.categoryName}
          </Typography>
          <Typography gutterBottom variant="h5" component="div">
            {cutText(courses.courseName)}
          </Typography>
          <Typography variant="h6" mt={10} color={"#790B0A"}>
            {formatCurrency(courses.coursePrice)}
          </Typography>
        </CardContent>
      </Card>
    </Link>
  );
}
