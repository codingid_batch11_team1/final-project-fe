import React, { useState } from "react";
import { Box, Button, Stack, Typography, Modal } from "@mui/material";
import { mainColor } from "../Colors";
import CheckCircleIcon from "@mui/icons-material/CheckCircle";
import usePostCheckoutPay from "../Hooks/usePostCheckoutPay";
import useGetPayment from "../Hooks/useGetPayment";

const style = {
  display: "flex",
  flexDirection: "column",
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  borderRadius: "10px",
  boxShadow: 24,
  p: 4,
};

function PaymentModal({
  open,
  handleClose,
  checkouts,
  selectedCheckboxes,
  schedule,
}) {
  const [selectedPaymentMethod, setSelectedPaymentMethod] = useState(null);
  const { isPaying, paymentError, pay } = usePostCheckoutPay();
  const { payments } = useGetPayment();

  // Function to handle the selection of a payment method
  const selectPaymentMethod = (payment) => {
    setSelectedPaymentMethod(payment.methodId);
  };

  const constructSelectedData = () => {
    // const idUser = 3; // Replace with actual logic to obtain user ID
    const idPaymentMethod = selectedPaymentMethod;
    const idCheckouts = [];
    const idSchedules = [];

    if (selectedCheckboxes) {
      for (const [id, isChecked] of Object.entries(selectedCheckboxes)) {
        if (isChecked) {
          const parsedId = parseInt(id);
          idCheckouts.push(parsedId);
          const checkout = checkouts.find((c) => c.idCheckout === parsedId);
          if (checkout?.fkIdSchedule) {
            idSchedules.push(checkout.fkIdSchedule);
          }
        }
      }
    } else {
      idSchedules.push(schedule);
    }

    return {
      idSchedules,
      idPaymentMethod,
      idCheckouts,
    };
  };

  const handlePaymentSubmission = async () => {
    // Ensure selectedCheckboxes is an object before constructing selected data
    if (selectedPaymentMethod) {
      const selectedData = constructSelectedData();
      await pay(selectedData);
      handleClose(); // Close the modal on successful payment
    } else {
      alert("No payment method selected");
      // Handle the error case where no payment method is selected or selectedCheckboxes is not defined
    }
  };

  return (
    <Modal open={open} onClose={handleClose}>
      <Box sx={style} mt={-7}>
        <Typography variant="h6" component="h2" sx={{ textAlign: "center" }}>
          Select Payment Method
        </Typography>
        <Stack
          direction={"column"}
          sx={{ marginTop: "24px" }}
          spacing={2}
          alignItems={"flex-start"}
        >
          {payments.map((payment) => (
            <Button
              key={payment.methodId}
              onClick={() => selectPaymentMethod(payment)}
              sx={{
                width: "100%", // Ensures the button stretches to fill the width
                justifyContent: "flex-start", // Aligns the button's content to the left
              }}
            >
              <Stack direction={"row"} spacing={2} alignItems={"center"}>
                {selectedPaymentMethod === payment.methodId && ( // This will only render if the payment method is selected
                  <CheckCircleIcon sx={{ color: mainColor }} />
                )}
                <img
                  src={`${import.meta.env.VITE_API_URL}${payment.methodImage}`}
                  alt={payment.methodName}
                  style={{ width: "40px", height: "40px" }}
                />
                <Typography
                  noWrap
                  sx={{
                    color: "black",
                  }}
                >
                  {payment.methodName}
                </Typography>
              </Stack>
            </Button>
          ))}
        </Stack>
        <Stack direction={"row"} justifyContent={"center"} spacing={2} mt={4}>
          <Button
            onClick={handleClose}
            variant="outline"
            sx={{
              color: "black",
              border: "1px solid black",
              marginX: "10px",
              flexGrow: 1,
            }}
          >
            Cancel
          </Button>
          <Button
            onClick={handlePaymentSubmission}
            sx={{
              backgroundColor: mainColor,
              color: "white",
              flexGrow: 1,
              "&:hover": {
                backgroundColor: mainColor,
                boxShadow: "0 0 5px rgba(255, 255, 255, 0.5)", // Adjust the shadow as needed
              },
            }}
          >
            Pay Now
          </Button>
        </Stack>
      </Box>
    </Modal>
  );
}

export default PaymentModal;
