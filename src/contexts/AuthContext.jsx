import Cookies from "js-cookie";
import {jwtDecode} from "jwt-decode";
import PropTypes from "prop-types";
import { createContext, useContext, useEffect, useState } from "react";
import { ACCESS_TOKEN } from "../utils/globalType";

export const AuthContext = createContext(null);

export function useAuth() {
  return useContext(AuthContext);
}

export const AuthProvider = ({ children }) => {
  const [isAuth, setIsAuth] = useState(Cookies.get("isAuth") === "true");
  const [token, setToken] = useState(Cookies.get(ACCESS_TOKEN));
  const [role, setRole] = useState('');

  const cookieOptions = {
    expires: new Date(new Date().getTime() + 60 * 60 * 1000), // 60 minutes * 60 seconds * 1000 milliseconds
  };

  const loginUser = (accessToken) => {
    Cookies.set("isAuth", "true", cookieOptions);
    Cookies.set(ACCESS_TOKEN, accessToken, cookieOptions);
    setToken(accessToken);
    setIsAuth(true);
    getUserRole();
  };

  const logout = () => {
    Cookies.remove("isAuth");
    Cookies.remove(ACCESS_TOKEN);
    setToken("");
    setIsAuth(false);
    window.location.reload();
  };

  function getUserRole() {
    const token = Cookies.get('ACCESS_TOKEN');
    if (!token) return null;
  
    try {
      const decoded = jwtDecode(token);
      setRole(decoded['http://schemas.microsoft.com/ws/2008/06/identity/claims/role']);
    } catch (error) {
      console.error('Error decoding token:', error);
      return null;
    }
  }

  useEffect(() => {
    getUserRole()
  }, [token]);
  
  return (
    <AuthContext.Provider value={{ isAuth, token, loginUser, logout, role }}>
      {children}
    </AuthContext.Provider>
  );
};

AuthProvider.propTypes = {
  children: PropTypes.node.isRequired,
};
