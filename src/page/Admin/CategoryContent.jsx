// import PaymentTableAdmin from "../../components/Admin/PaymentTableAdmin";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Modal from "@mui/material/Modal";
import Radio from "@mui/material/Radio";
import RadioGroup from "@mui/material/RadioGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import FormControl from "@mui/material/FormControl";
import FormLabel from "@mui/material/FormLabel";
import { Stack, TextField } from "@mui/material";
// import useAddUser from "../../hooks/useAddUser";
import axios from "axios";
import { useContext, useState } from "react";
import CategoryTableAdmin from "../../component/Admin/CategoryTableAdmin";
import { AuthContext } from "../../contexts/AuthContext";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

function CourseContent() {
  const [open, setOpen] = useState(false);
  const [categoryImage, setCategoryImage] = useState(null);
  const [categoryBanner, setCategoryBanner] = useState(null);
  const [categoryDetail, setCategoryDetail] = useState("");
  const [categoryName, setCategoryName] = useState("");
  const [categoryStatus, setCategoryStatus] = useState("true");
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const { token } = useContext(AuthContext);

  const handleImageChange = (event) => {
    const selectedFile = event.target.files[0];

    if (selectedFile) {
      const allowedFormats = ["image/jpeg", "image/png"];
      if (allowedFormats.includes(selectedFile.type)) {
        setCategoryImage(selectedFile);
      } else {
        // Display an error message or perform any other action
        // console.log(
        //   "Invalid file format. Please select a PNG or JPG image file."
        // );
      }
    }
  };

  const handleBannerChange = (event) => {
    const selectedFile = event.target.files[0];

    if (selectedFile) {
      const allowedFormats = ["image/jpeg", "image/png"];
      if (allowedFormats.includes(selectedFile.type)) {
        setCategoryBanner(selectedFile);
      } else {
        // Display an error message or perform any other action
        // console.log(
        //   "Invalid file format. Please select a PNG or JPG image file."
        // );
      }
    }
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    try {
      const formData = new FormData();
      formData.append("categoryName", categoryName);
      formData.append("categoryDetail", categoryDetail);
      formData.append("categoryImage", categoryImage);
      formData.append("categoryBanner", categoryBanner);
      formData.append("isActive", categoryStatus);

      await axios.post(
        `${import.meta.env.VITE_API_URL}/api/CourseCategory`,
        formData,
        {
          headers: {
            "Content-Type": "multipart/form-data",
            Authorization: `Bearer ${token}`,
          },
        }
      );

      window.alert("Category added successfully!");
      handleClose();
      window.location.reload();
    } catch (error) {
      console.error("Error adding payment:", error);
    }
  };

  const handleStatusChange = (event) => {
    setCategoryStatus(event.target.value);
  };

  // console.log(categoryName);
  // console.log(categoryDetail);
  // console.log(categoryStatus);
  // console.log(categoryImage);
  // console.log(categoryBanner);

  // console.log(paymentStatus);
  // console.log(logoFile);

  return (
    <div>
      <h2>Category Content</h2>
      <Button
        variant="contained"
        onClick={handleOpen}
        sx={{
          marginTop: "10px",
          marginBottom: "20px",
          background: "#FABC1D",
          color: "#5B4947",
          fontFamily: "Montserrat",
          textTransform: "capitalize",
          fontWeight: "700",
        }}
      >
        Add Category
      </Button>
      <Modal open={open} onClose={handleClose}>
        <Box sx={style}>
          <FormControl>
            <Stack spacing={2}>
              <Typography variant="h6" component="h2">
                Add Category
              </Typography>
              <TextField
                label="Course Name"
                size="small"
                variant="outlined"
                value={categoryName}
                onChange={(e) => setCategoryName(e.target.value)}
              />
              <TextField
                label="Detail"
                size="small"
                variant="outlined"
                value={categoryDetail}
                onChange={(e) => setCategoryDetail(e.target.value)}
              />
              <Stack spacing={2}>
                <Typography>Course Image</Typography>
                <TextField
                  type="file"
                  size="small"
                  variant="outlined"
                  accept="image/*"
                  onChange={handleImageChange}
                />
                <Typography>Course Banner</Typography>
                <TextField
                  type="file"
                  size="small"
                  variant="outlined"
                  accept="image/*"
                  onChange={handleBannerChange}
                />
              </Stack>
              <FormControl component="fieldset">
                <FormLabel component="legend">Status</FormLabel>
                <RadioGroup
                  row
                  value={categoryStatus}
                  onChange={handleStatusChange}
                >
                  <FormControlLabel
                    value="true"
                    control={<Radio />}
                    label="Active"
                  />
                  <FormControlLabel
                    value="false"
                    control={<Radio />}
                    label="Inactive"
                  />
                </RadioGroup>
              </FormControl>
              <Button
                variant="contained"
                color="success"
                onClick={handleSubmit}
              >
                Add
              </Button>
            </Stack>
          </FormControl>
        </Box>
      </Modal>
      <CategoryTableAdmin />
    </div>
  );
}

export default CourseContent;
