// import PaymentTableAdmin from "../../components/Admin/PaymentTableAdmin";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Modal from "@mui/material/Modal";
import Radio from "@mui/material/Radio";
import RadioGroup from "@mui/material/RadioGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import InputLabel from "@mui/material/InputLabel";
import Select from "@mui/material/Select";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import FormLabel from "@mui/material/FormLabel";
import { Stack, TextField } from "@mui/material";
// import useAddUser from "../../hooks/useAddUser";
import axios from "axios";
import { useContext, useState } from "react";
import CourseTableAdmin from "../../component/Admin/CourseTableAdmin";
import useGetCategory from "../../component/Hooks/useGetCategory";
import { AuthContext } from "../../contexts/AuthContext";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

function CourseContent() {
  const [open, setOpen] = useState(false);
  const [courseImage, setCourseImage] = useState(null);
  const [courseName, setCourseName] = useState("");
  const [courseDetail, setCourseDetail] = useState("");
  const [categoryName, setCategoryName] = useState("");
  const [coursePrice, setCoursePrice] = useState("");
  const [courseStatus, setCourseStatus] = useState("true");
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const { token } = useContext(AuthContext);

  const { data } = useGetCategory();

  const handleLogoChange = (e) => {
    const file = e.target.files[0];

    if (file) {
      setCourseImage(file);
    }
  };

  //   const handleImageChange = (e) => {
  //     const file = e.target.files[0];

  //     if (file) {
  //         const imageUrl = URL.createObjectURL(file);
  //         setShowImage(imageUrl)
  //         setImagePath(file);
  //     }
  // };

  const handleSubmit = async (event) => {
    event.preventDefault();

    try {
      const formData = new FormData();
      formData.append("courseName", courseName);
      formData.append("courseDetail", courseDetail);
      formData.append("coursePrice", coursePrice);
      formData.append("fk_Id_Category", categoryName);
      formData.append("isActive", courseStatus);

      if (courseImage) {
        formData.append("courseImage", courseImage);
      }

      await axios.post(`${import.meta.env.VITE_API_URL}/api/Course`, formData, {
        headers: {
          "Content-Type": "multipart/form-data",
          Authorization: `Bearer ${token}`,
        },
      });

      window.alert("Course added successfully!");
      handleClose();
      window.location.reload();
    } catch (error) {
      window.alert("Payment failed to add");
      console.error("Error adding payment:", error);
    }
  };

  const handleStatusChange = (event) => {
    setCourseStatus(event.target.value);
  };

  // console.log(courseName);
  // console.log(categoryName);
  // console.log(courseStatus);
  // console.log(courseImage);
  // console.log(courseDetail);

  // console.log(paymentStatus);
  // console.log(logoFile);

  return (
    <div>
      <h2>Course Content</h2>
      <Button
        variant="contained"
        onClick={handleOpen}
        sx={{
          marginTop: "10px",
          marginBottom: "20px",
          background: "#FABC1D",
          color: "#5B4947",
          fontFamily: "Montserrat",
          textTransform: "capitalize",
          fontWeight: "700",
        }}
      >
        Add Course
      </Button>
      <Modal open={open} onClose={handleClose}>
        <Box sx={style}>
          <FormControl>
            <Stack spacing={2}>
              <Typography variant="h6" component="h2">
                Add Course
              </Typography>
              <TextField
                label="Course Name"
                size="small"
                variant="outlined"
                value={courseName}
                onChange={(e) => setCourseName(e.target.value)}
              />
              <TextField
                label="Detail"
                size="small"
                variant="outlined"
                value={courseDetail}
                onChange={(e) => setCourseDetail(e.target.value)}
              />
              <TextField
                label="Price"
                size="small"
                variant="outlined"
                value={coursePrice}
                onChange={(e) => setCoursePrice(e.target.value)}
              />
              <TextField
                type="file"
                size="small"
                variant="outlined"
                accept="image/*"
                onChange={handleLogoChange}
              />
              <FormControl fullWidth>
                <InputLabel id="demo-simple-select-label">Category</InputLabel>
                <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  value={categoryName}
                  label="Category"
                  onChange={(e) => setCategoryName(e.target.value)}
                >
                  {data.map((row) => (
                    <MenuItem key={row.idCategory} value={row.idCategory}>
                      {row.categoryName}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>
              <FormControl component="fieldset">
                <FormLabel component="legend">Status</FormLabel>
                <RadioGroup
                  row
                  value={courseStatus}
                  onChange={handleStatusChange}
                >
                  <FormControlLabel
                    value="true"
                    control={<Radio />}
                    label="Active"
                  />
                  <FormControlLabel
                    value="false"
                    control={<Radio />}
                    label="Inactive"
                  />
                </RadioGroup>
              </FormControl>
              <Button
                variant="contained"
                color="success"
                onClick={handleSubmit}
              >
                Add
              </Button>
            </Stack>
          </FormControl>
        </Box>
      </Modal>
      <CourseTableAdmin />
    </div>
  );
}

export default CourseContent;
