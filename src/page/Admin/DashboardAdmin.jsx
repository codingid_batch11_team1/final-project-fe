import * as React from "react";
import PropTypes from "prop-types";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import CssBaseline from "@mui/material/CssBaseline";
import Divider from "@mui/material/Divider";
import Drawer from "@mui/material/Drawer";
import IconButton from "@mui/material/IconButton";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import MenuIcon from "@mui/icons-material/Menu";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import DashboardContent from "./DasboardContent";
import UserContent from "./UserContent";
import InvoiceContent from "./InvoiceContent";
import PaymentContent from "./PaymentContent";
import LogoutIcon from "@mui/icons-material/Logout";
import { useContext } from "react";
import { AuthContext } from "../../contexts/AuthContext";
import DashboardIcon from "@mui/icons-material/Dashboard";
import PersonIcon from "@mui/icons-material/Person";
import PaymentIcon from "@mui/icons-material/Payment";
import ReceiptIcon from "@mui/icons-material/Receipt";
import CourseContent from "./CourseContent";
import CreateNewFolderIcon from "@mui/icons-material/CreateNewFolder";
import CategoryContent from "./CategoryContent";
import CategoryIcon from "@mui/icons-material/Category";

const drawerWidth = 240;

function DashboardAdmin(props) {
  const { window } = props;
  const [mobileOpen, setMobileOpen] = React.useState(false);
  const [selectedContent, setSelectedContent] = React.useState("Dashboard");

  const { logout } = useContext(AuthContext);
  //   const { logoutButtonFunc } = useLogout();

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  const handleSidebarItemClick = (content) => {
    setSelectedContent(content);
    handleDrawerToggle();
  };

  const contentComponents = {
    Dashboard: <DashboardContent />,
    User: <UserContent />,
    Payment: <PaymentContent />,
    Invoice: <InvoiceContent />,
    Course: <CourseContent />,
    Category: <CategoryContent />,
  };

  const icons = [
    <DashboardIcon key="dashboard" />,
    <PersonIcon key="person" />,
    <PaymentIcon key="payment" />,
    <ReceiptIcon key="receipt" />,
    <CreateNewFolderIcon key="course" />,
    <CategoryIcon key="category" />,
  ];

  const drawer = (
    <div>
      <Toolbar />
      <Divider />
      <List>
        {["Dashboard", "User", "Payment", "Invoice", "Course", "Category"].map(
          (text, index) => (
            <ListItem
              key={text}
              disablePadding
              onClick={() => handleSidebarItemClick(text)}
            >
              <ListItemButton>
                <ListItemIcon>{icons[index]}</ListItemIcon>
                <ListItemText primary={text} />
              </ListItemButton>
            </ListItem>
          )
        )}
      </List>
      <Divider />
    </div>
  );

  const container =
    window !== undefined ? () => window().document.body : undefined;

  return (
    <Box sx={{ display: "flex" }}>
      <CssBaseline />
      <AppBar
        position="fixed"
        sx={{
          width: { sm: `calc(100% - ${drawerWidth}px)` },
          ml: { sm: `${drawerWidth}px` },
          backgroundColor: "#5B4947",
        }}
      >
        <Toolbar sx={{ display: "flex", justifyContent: "space-between" }}>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            edge="start"
            onClick={handleDrawerToggle}
            sx={{ mr: 2, display: { sm: "none" } }}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" noWrap component="div">
            Dashboard Admin
          </Typography>
          <LogoutIcon onClick={() => logout()} />
        </Toolbar>
      </AppBar>
      <Box
        component="nav"
        sx={{ width: { sm: drawerWidth }, flexShrink: { sm: 0 } }}
        aria-label="mailbox folders"
      >
        <Drawer
          container={container}
          variant="temporary"
          open={mobileOpen}
          onClose={handleDrawerToggle}
          ModalProps={{
            keepMounted: true,
          }}
          sx={{
            display: { xs: "block", sm: "none" },
            "& .MuiDrawer-paper": {
              boxSizing: "border-box",
              width: drawerWidth,
            },
          }}
        >
          {drawer}
        </Drawer>
        <Drawer
          variant="permanent"
          sx={{
            display: { xs: "none", sm: "block" },
            "& .MuiDrawer-paper": {
              boxSizing: "border-box",
              width: drawerWidth,
            },
          }}
          open
        >
          {drawer}
        </Drawer>
      </Box>
      <Box
        component="main"
        sx={{
          flexGrow: 1,
          p: 3,
          width: { sm: `calc(100% - ${drawerWidth}px)` },
          overflowY: "auto",
          maxHeight: "100vh",
          marginTop: "50px",
        }}
      >
        <div style={{ padding: "16px" }}>
          {contentComponents[selectedContent]}
          <Toolbar />
        </div>
      </Box>
    </Box>
  );
}

DashboardAdmin.propTypes = {
  window: PropTypes.func,
};

export default DashboardAdmin;
