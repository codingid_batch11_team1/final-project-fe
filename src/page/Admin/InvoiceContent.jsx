import InvoiceTableAdmin from "../../component/Admin/InvoiceTableAdmin";

function InvoiceContent() {
  return (
    <div>
      <h2>Invoice Content</h2>
      <p>View and manage invoices for your users.</p>
      <InvoiceTableAdmin />
    </div>
  );
}

export default InvoiceContent;
