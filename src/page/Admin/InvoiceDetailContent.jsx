/* eslint-disable no-unused-vars */
import React from "react";
import InvoiceDetailAdmin from "../../component/Admin/InvoiceDetailAdmin";
import { textFieldClasses } from "@mui/material";

function InvoiceDetailContent() {
  return (
    <div>
      <h2 style={{ textAlign: "center" }}>Invoice Details Content</h2>
      <InvoiceDetailAdmin />
    </div>
  );
}

export default InvoiceDetailContent;
