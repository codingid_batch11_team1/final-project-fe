// import PaymentTableAdmin from "../../components/Admin/PaymentTableAdmin";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Modal from "@mui/material/Modal";
import Radio from "@mui/material/Radio";
import RadioGroup from "@mui/material/RadioGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import FormControl from "@mui/material/FormControl";
import FormLabel from "@mui/material/FormLabel";
import { Stack, TextField } from "@mui/material";
// import useAddUser from "../../hooks/useAddUser";
import axios from "axios";
import { useContext, useState } from "react";
import PaymentTableAdmin from "../../component/Admin/PaymentTableAdmin";
import { AuthContext } from "../../contexts/AuthContext";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

function PaymentContent() {
  const [open, setOpen] = useState(false);
  const [paymentImage, setPaymentImage] = useState(null);
  const [paymentName, setPaymentName] = useState("");
  const [paymentStatus, setPaymentStatus] = useState("true");
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const { token } = useContext(AuthContext);

  const handleLogoChange = (event) => {
    const selectedFile = event.target.files[0];

    if (selectedFile) {
      const allowedFormats = ["image/jpeg", "image/png"];
      if (allowedFormats.includes(selectedFile.type)) {
        setPaymentImage(selectedFile);
      } else {
        // Display an error message or perform any other action
        // console.log(
        //   "Invalid file format. Please select a PNG or JPG image file."
        // );
      }
    }
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    try {
      const formData = new FormData();
      formData.append("methodName", paymentName);
      formData.append("methodImage", paymentImage);
      formData.append("isActive", paymentStatus);

      await axios.post(
        `${import.meta.env.VITE_API_URL}/api/payment-methods`,
        formData,
        {
          headers: {
            "Content-Type": "multipart/form-data",
            Authorization: `Bearer ${token}`,
          },
        }
      );

      window.alert("Payment added successfully!");
      handleClose();
      window.location.reload();
    } catch (error) {
      console.error("Error adding payment:", error);
      window.alert("Payment failed to add");
    }
  };

  const handleStatusChange = (event) => {
    setPaymentStatus(event.target.value);
  };

  // console.log(paymentName);
  // console.log(paymentStatus);
  // console.log(logoFile);

  return (
    <div>
      <h2>Payment Content</h2>
      <Button
        variant="contained"
        onClick={handleOpen}
        sx={{
          marginTop: "10px",
          marginBottom: "20px",
          background: "#FABC1D",
          color: "#5B4947",
          fontFamily: "Montserrat",
          textTransform: "capitalize",
          fontWeight: "700",
        }}
      >
        Add Payment
      </Button>
      <Modal open={open} onClose={handleClose}>
        <Box sx={style}>
          <FormControl>
            <Stack spacing={2}>
              <Typography variant="h6" component="h2">
                Add Payment
              </Typography>
              <TextField
                label="Payment Name"
                size="small"
                variant="outlined"
                value={paymentName}
                onChange={(e) => setPaymentName(e.target.value)}
              />
              <TextField
                type="file"
                size="small"
                variant="outlined"
                accept="image/*"
                onChange={handleLogoChange}
              />
              <FormControl component="fieldset">
                <FormLabel component="legend">Status</FormLabel>
                <RadioGroup
                  row
                  value={paymentStatus}
                  onChange={handleStatusChange}
                >
                  <FormControlLabel
                    value="true"
                    control={<Radio />}
                    label="Active"
                  />
                  <FormControlLabel
                    value="false"
                    control={<Radio />}
                    label="Inactive"
                  />
                </RadioGroup>
              </FormControl>
              <Button
                variant="contained"
                color="success"
                onClick={handleSubmit}
              >
                Add
              </Button>
            </Stack>
          </FormControl>
        </Box>
      </Modal>
      <PaymentTableAdmin />
    </div>
  );
}

export default PaymentContent;
