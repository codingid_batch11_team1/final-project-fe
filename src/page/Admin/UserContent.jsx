import React from "react";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Modal from "@mui/material/Modal";
import Radio from "@mui/material/Radio";
import RadioGroup from "@mui/material/RadioGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import FormControl from "@mui/material/FormControl";
import FormLabel from "@mui/material/FormLabel";
import { Stack, TextField } from "@mui/material";
import UserTableAdmin from "../../component/Admin/UserTableAdmin";
import useAddUserByAdmin from "../../component/Hooks/useAddUserByAdmin";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

function UserContent() {
  const [open, setOpen] = React.useState(false);
  const [nameError, setNameError] = React.useState("");
  const [emailError, setEmailError] = React.useState("");
  const [passwordError, setPasswordError] = React.useState("");
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const { submitUser, userName, password, email, role, status, setUserName, setEmail, setPassword, setRole, setStatus } = useAddUserByAdmin();

  const handleNameChange = (e) => {
    setUserName(e.target.value);
    setNameError(e.target.value === "" ? "Name is required" : "");
  };

  const handleEmailChange = (e) => {
    setEmail(e.target.value);
    setEmailError(e.target.value === "" ? "Email is required" : "");
  };

  const handlePasswordChange = (e) => {
    setPassword(e.target.value);
    setPasswordError(e.target.value === "" ? "Password is required" : "");
  };

  const handleRole = (event) => {
    setRole(event.target.value);
  };

  const handleStatus = (event) => {
    setStatus(event.target.value === "true");
  };


  // console.log(userData);

  

  //   const { submitUser } = useAddUser();

  const handleSubmit = (event) => {
    event.preventDefault();
  
    // Regular expression for a simple email validation
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
  
    if (
      userName.trim() !== "" &&
      email.trim() !== "" &&
      emailRegex.test(email) &&
      password.trim() !== "" &&
      password.trim().length >= 8 && // Ensure password is at least 8 characters
      role !== "" &&
      status !== ""
    ) {
      submitUser();
      handleClose();
      window.alert("User added successfully!");
      window.location.reload();
    } else {
      setNameError(userName.trim() === "" ? "Name is required" : "");
      setEmailError(email.trim() === "" ? "Email is required" : "");
      setEmailError(!emailRegex.test(email.trim()) ? "Invalid email format" : "");
      setPasswordError(password.trim() === "" ? "Password is required" : "");
      setPasswordError(
        password.trim().length < 8 ? "Password must be at least 8 characters" : ""
      );
    }
  };
  

  return (
    <Box>
      <h2>User Page</h2>
      <Button
        variant="contained"
        onClick={handleOpen}
        sx={{
          marginTop: "10px",
          marginBottom: "20px",
          background: "#FABC1D",
          color: "#5B4947",
          fontFamily: "Montserrat",
          textTransform: "capitalize",
          fontWeight: "700",
        }}
      >
        Add User
      </Button>
      <Modal open={open} onClose={handleClose}>
        <Box sx={style}>
          <FormControl onSubmit={handleSubmit}>
            <Stack spacing={2}>
              <Typography variant="h6" component="h2">
                Add User
              </Typography>
              <TextField
                label="User Name"
                size="small"
                variant="outlined"
                value={userName}
                error={nameError !== ""}
                helperText={nameError}
                onChange={handleNameChange}
              />
              <TextField
                label="Email"
                size="small"
                variant="outlined"
                value={email}
                error={emailError !== ""}
                helperText={emailError}
                onChange={handleEmailChange}
              />
              <TextField
                label="Password"
                type="password"
                size="small"
                variant="outlined"
                value={password}
                error={passwordError !== ""}
                helperText={passwordError}
                onChange={handlePasswordChange}
              />
              <FormControl component="fieldset">
                <FormLabel component="legend">Role</FormLabel>
                <RadioGroup
                  row
                  aria-labelledby="demo-row-radio-buttons-group-label"
                  name="controlled-radio-buttons-group"
                  value={role}
                  onChange={handleRole}
                >
                  <FormControlLabel
                    value={"ADMIN"}
                    control={<Radio />}
                    label="Admin"
                  />
                  <FormControlLabel
                    value={"USER"}
                    control={<Radio />}
                    label="User"
                  />
                </RadioGroup>
              </FormControl>
              <FormControl component="fieldset">
                <FormLabel component="legend">Status</FormLabel>
                <RadioGroup
                  row
                  aria-labelledby="demo-row-radio-buttons-group-label"
                  name="row-radio-buttons-group"
                  value={status}
                  onChange={handleStatus}
                >
                  <FormControlLabel
                    value={true}
                    control={<Radio />}
                    label="Active"
                  />
                  <FormControlLabel
                    value={false}
                    control={<Radio />}
                    label="Inactive"
                  />
                </RadioGroup>
              </FormControl>
              <Button
                variant="contained"
                color="success"
                onClick={handleSubmit}
              >
                Add
              </Button>
            </Stack>
          </FormControl>
        </Box>
      </Modal>
      <UserTableAdmin />
    </Box>
  );
}

export default UserContent;
