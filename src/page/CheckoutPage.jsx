import Checkout from "../component/Checkout";
import CheckoutCard from "../component/CheckoutCard";
import CheckoutConfirm from "../component/CheckoutConfirm";
import { useState, useEffect } from "react";
import useGetCheckout from "../component/Hooks/useGetCheckout";
import useRemoveCheckoutById from "../component/Hooks/useRemoveCheckoutById";
import PaymentModal from "../component/modal/PaymentModal";

export default function CheckoutPage() {
  const [selectAll, setSelectAll] = useState(false);
  const [selectedCheckboxes, setSelectedCheckboxes] = useState({});
  const [checkoutRemoved, setCheckoutRemoved] = useState(false);
  const { checkouts, error, isLoading, getCheckout } = useGetCheckout();
  const { removeCheckout } = useRemoveCheckoutById();
  const [openModal, setOpenModal] = useState(false);

  // Initialize selectedCheckboxes state based on fetched checkouts
  useEffect(() => {
    if (checkoutRemoved) {
      getCheckout(); // This function would re-fetch the checkouts
      setCheckoutRemoved(false);
    } else {
      // Initialize selectedCheckboxes state based on fetched checkouts
      const initialCheckboxesState = {};
      checkouts.forEach(checkout => {
        initialCheckboxesState[checkout.idCheckout] = false;
      });
      setSelectedCheckboxes(initialCheckboxesState);
    }
  }, [checkoutRemoved, checkouts]);

  const handleCheckboxChange = (id, checked) => {
    const newSelectedCheckboxes = { ...selectedCheckboxes, [id]: checked };
    setSelectedCheckboxes(newSelectedCheckboxes);
    
    // Check if all individual checkboxes are checked
    const allChecked = Object.values(newSelectedCheckboxes).every(Boolean);
    setSelectAll(allChecked);
  };
  
  const handleSelectAllChange = (event) => {
    const newSelectedCheckboxes = {};
    checkouts.forEach(checkout => {
      newSelectedCheckboxes[checkout.idCheckout] = event.target.checked;
    });
    setSelectedCheckboxes(newSelectedCheckboxes);
    setSelectAll(event.target.checked);
  };

  // Function to pass to the CheckoutCard component
  const handleRemoveCheckout = async (idCheckout) => {
    await removeCheckout(idCheckout);
    setCheckoutRemoved(true); // Trigger re-render
  };

  if (isLoading) return <div>Loading...</div>;
  if (error) return <div>Error: {error.message}</div>;

  const handleOpenPaymentModal = () => {
    setOpenModal(true);
  };

  // console.log(selectAll);
  // console.log(selectedCheckboxes);

  return (
    <>
      <Checkout selectAll={selectAll} onSelectAllChange={handleSelectAllChange} />
      <CheckoutCard
        checkouts={checkouts}
        selectedCheckboxes={selectedCheckboxes}
        onCheckboxChange={handleCheckboxChange}
        selectAll={selectAll}
        onDeleteCheckout={handleRemoveCheckout}
      />
      <CheckoutConfirm
        checkouts={checkouts}
        selectedCheckboxes={selectedCheckboxes}
        onOpenPaymentModal={handleOpenPaymentModal} // Add this line
      />
      {checkouts && selectedCheckboxes && (
        <PaymentModal
          open={openModal}
          handleClose={() => setOpenModal(false)}
          checkouts={checkouts}
          selectedCheckboxes={selectedCheckboxes}
        />
      )}
    </>
  );
}
