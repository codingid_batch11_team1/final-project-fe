/* eslint-disable no-unused-vars */
import ClassDetail from "../component/ClassDetail";
import { Divider, Typography } from "@mui/material";
import LandingList from "../component/LandingList";
import { useParams } from "react-router-dom";
import { useEffect, useState } from "react";
import axios from "axios";
import Footer from "../component/Footer"

export default function ClassDetailPage() {

    return (
        <>
            <ClassDetail />
            <Divider />
            <Typography textAlign='center' variant='h4' mt={7} mb={7}>
                Another favorite course
            </Typography>
            <LandingList />
            <Divider />
            <Footer />

        </>
    );
}