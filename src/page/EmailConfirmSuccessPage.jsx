
import EmailConfirmSuccess from '../component/EmailConfirmSuccess'
import HeaderEmailConfirmSuccess from '../component/HeaderEmailConfirmSuccess'

export default function EmailConfirmSuccessPage() {
    return (
        <>
            <HeaderEmailConfirmSuccess />
            <EmailConfirmSuccess />
        </>
    )
}