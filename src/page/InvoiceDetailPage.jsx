import InvoiceDetail from "../component/Invoice/InvoiceDetails";
import Footer from "../component/Footer"

export default function InvoiceDetailPage() {
  return (
    <>
      <InvoiceDetail />
      <Footer />
    </>
  );
}
