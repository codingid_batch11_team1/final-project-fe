import InvoiceTable from "../component/Invoice/InvoiceTable";
import Footer from "../component/Footer"

export default function InvoicePage() {
  return (
    <>
      <InvoiceTable />
      <Footer />
    </>
  );
}
