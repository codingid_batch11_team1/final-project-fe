import LandingList from "../component/LandingList";
import { Divider, Typography } from "@mui/material";
import LandingListMenuClass from "../component/LandingListMenuClass";
import Footer from "../component/Footer";

export default function LandingListMenuClassPage() {
  return (
    <>
      <LandingListMenuClass />
      <Divider />
      <Typography textAlign="center" variant="h4" mt={7} mb={7}>
        Another favorite course
      </Typography>
      <LandingList />
      <Divider />
      <Footer />
    </>
  );
}
