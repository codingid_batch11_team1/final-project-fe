//import Header from "../component/Header";
import LandingCard from '../component/LandingCard'
import LandingBenefit from "../component/LandingBenefit";
import LandingList from "../component/LandingList";
import LandingKategori from "../component/LandingKategori";
import Footer from "../component/Footer";
import { Divider, Typography } from "@mui/material";

export default function LandingPage() {
    return (
        <>
            <LandingCard />
            <Typography textAlign='center' variant='h4' mt={7} mb={7}>Join us for the course</Typography>
            <LandingList />
            <LandingBenefit />
            <LandingKategori />
            <Divider />
            <Footer />
        </>
    )
}