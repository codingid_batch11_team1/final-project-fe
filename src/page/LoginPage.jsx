/* eslint-disable no-unused-vars */
import LoginForm from "../component/LoginForm";
import { AuthContext } from "../contexts/AuthContext";
import { useContext } from "react";

export default function LoginPage() {
  const { isAuth } = useContext(AuthContext);

  return (
    <>
      <LoginForm />
    </>
  );
}
