import MyClass from "../component/MyClass";
import Footer from "../component/Footer"

export default function MyClassPage() {
  return (
    <>
      <MyClass />
      <Footer />
    </>
  );
}
