
import HeaderEmailConfirmSuccess from '../component/HeaderEmailConfirmSuccess'
import PurchaseSuccess from '../component/PurchaseSuccess'

export default function EmailConfirmSuccessPage() {
    return (
        <>
            <HeaderEmailConfirmSuccess />
            <PurchaseSuccess/>
        </>
    )
}